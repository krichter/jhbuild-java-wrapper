/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.jhbuild.java.wrapper.download;

/**
 *
 * @author richter
 */
@FunctionalInterface
public interface DownloadEmptyCallback {
    DownloadEmptyCallback RETRY_5_TIMES = numberOfRetries -> {
        return numberOfRetries < 5
                ? DownloadEmptyCallbackReation.RETRY
                : DownloadEmptyCallbackReation.CANCEL;
    };

    static DownloadEmptyCallback fail() {
        return new DownloadEmptyCallback() {
            @Override
            public DownloadEmptyCallbackReation run(int numberOfRetries) {
                throw new DownloadEmptyException();
            }
        };
    }

    static DownloadEmptyCallback failAfterRetries(int numberOfRetriesMax) {
        return new DownloadEmptyCallback() {
            @Override
            public DownloadEmptyCallbackReation run(int numberOfRetries) {
                if(numberOfRetries <= numberOfRetriesMax) {
                    return DownloadEmptyCallbackReation.RETRY;
                }
                throw new DownloadEmptyException(numberOfRetries);
            }
        };
    }

    DownloadEmptyCallbackReation run(int numberOfRetries);
}
