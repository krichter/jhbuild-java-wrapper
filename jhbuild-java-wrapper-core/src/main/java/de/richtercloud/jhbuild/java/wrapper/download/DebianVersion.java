/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.jhbuild.java.wrapper.download;

import java.util.Objects;

/**
 * A data class encapsulating information about the Debian version this software is executed on.
 */
public class DebianVersion {
    private final int major;

    public DebianVersion(int major) {
        this.major = major;
    }

    public int getMajor() {
        return major;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DebianVersion that = (DebianVersion) o;
        return major == that.major;
    }

    @Override
    public int hashCode() {
        return Objects.hash(major);
    }
}
