/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.jhbuild.java.wrapper;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableMap;
import de.richtercloud.execution.tools.BinaryUtils;
import de.richtercloud.execution.tools.BinaryValidationException;
import de.richtercloud.execution.tools.ExecutionUtils;
import de.richtercloud.execution.tools.OutputReaderThread;
import de.richtercloud.execution.tools.OutputReaderThreadMode;
import de.richtercloud.jhbuild.java.wrapper.download.*;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.io.UncheckedIOException;
import java.nio.channels.FileLock;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Stream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
internal implementation notes:
- it's troublesome to extract all installPrerequisites methods into a separate
class because it uses almost all properties of JHBuildJavaWrapper
*/
/**
 * A wrapper around GNOME's JHBuild build and dependency manager which allows
 * to automatic download of tarballs and checkout of source roots with various
 * SCMs, automatic build, test and installation into a user prefix as well as
 * specification and resolution of dependencies.
 *
 * The wrapper uses some system binaries which are expected to be present and
 * otherwise need to be installed manually by the caller (e.g. {@code sh} and
 * {@code make}. Other non-system binaries, like {@code git} and {@code jhbuild}
 * itself are searched for and automatically downloaded and installed in case of
 * absence. Both system and non-system binaries are search for in the
 * environment variable {@code PATH} and the subdirectory {@code bin} of the
 * specified {@code installationPrefix} so that it's possible to install them as
 * non-root user (system-binaries don't necessarily have to be installed
 * manually into {@code installationPrefix} since any other installation prefix
 * can be added to {@code PATH} before the wrapper is used).
 *
 * {@code stdout} and {@code stderr} of build processes are redirected to
 * {@code stdout} and {@code stderr} of the JVM except {@code silenceStdout} or
 * {@code silenceStderr} are set to {@code true}. In this case the content of
 * both process streams is wrapped in a {@link BuildFailureException} in case a
 * build process returns a code which is unequal {@code 0}.
 *
 * The Wrapper make JHBuild use it's default cache directory for downloads and
 * build results under the user's home directory since there's few incentive to
 * make the location configurable.
 *
 * Only the {@link #cancelInstallModuleset() } is allowed to be called from
 * another thread, calls to other methods from other threads result in
 * unpredictable behaviour.
 *
 * The initialization routine makes sure that a C compiler and {@code make} are
 * provided by the system since it's very hard or impossible to build a C
 * compiler without an existing one as well as building {@code make} without
 * {@code make}. The initialization routine then builds and installs {@code git}
 * and all of its prerequisites and then uses it to clone the {@code jhbuild}
 * repository and build and install it. A tarball of {@code jhbuild} could be
 * used, but that's a TODO.
 *
 * @author richter
 */
public class JHBuildJavaWrapper {
    private final static Logger LOGGER = LoggerFactory.getLogger(JHBuildJavaWrapper.class);
    public final static String GIT_DEFAULT = "git";
    public final static String JHBUILD_DEFAULT = "jhbuild";
    public final static String SH_DEFAULT = "bash";
    public final static String MAKE_DEFAULT = "make";
    public final static String PYTHON3_DEFAULT = "python3";
    public final static String CC_DEFAULT = "gcc";
    public final static String MSGFMT_DEFAULT = "msgfmt";
    public final static String CPAN_DEFAULT = "cpan";
    public final static String PATCH_DEFAULT = "patch";
    public final static String OPENSSL_DEFAULT = "openssl";
    public final static String WGET_DEFAULT = "wget";
    public final static String PKG_CONFIG_DEFAULT = "pkg-config";
    public final static String XZ_DEFAULT = "xz";
    public final static String M4_DEFAULT = "m4";
    public final static File CONFIG_DIR = new File(SystemUtils.getUserHome(),
            ".jhbuild-java-wrapper");
    public final static File INSTALLATION_PREFIX_DIR_DEFAULT = new File(CONFIG_DIR,
            "installation-prefix");
    public final static File DOWNLOAD_DIR_DEFAULT = new File(CONFIG_DIR,
            "downloads");
    private final static String PATH = "PATH";
    private final static String CONFIGURE = "configure";
    private final static String GIT_TEMPLATE = "git";
    private final static String JHBUILD_TEMPLATE = "jhbuild";
    private final static String JHBUILD_TEMPLATE_2 = "jhbuild ";
    private final static String PYTHON3_TEMPLATE = "python3";
    private final static String CPAN_TEMPLATE = "cpan";
    private final static String OPENSSL_TEMPLATE = "openssl";
    private final static String WGET_TEMPLATE = "wget";
    private final static String PKG_CONFIG_TEMPLATE = "pkg-config";
    private final static String GNUTLS_TEMPLATE = "gnutls";
    private final static String NETTLE_TEMPLATE = "nettle";
    private final static String XZ_TEMPLATE = "xz";
    private final static String M4_TEMPLATE = "m4";
    private final static String GMP_TEMPLATE = "gmp";
    private final static String MAKE_BINARY_NAME = "make";
    /* default */ final static String ZLIB_PC = "zlib.pc";
    /* default */ final static String OPENSSL_PC = "openssl.pc";
    /* default */ final static String GMP_PC = "gmp.pc";
    /* default */ final static String NETTLE_PC = "nettle.pc";
    /* default */ final static String LZMA_PC = "liblzma.pc";
    /* default */ final static String GNUTLS_PC = "gnutls.pc";
    private final static String REDIRECTED_TEMPLATE = "[redirected]";
    private static final String ACTION_TEMPLATE = "action %s not supported";
    private final static String OUTPUT_MODE_TEMPLATE = "output mode %s not supported";
    private static final File JHBUILD_FILE_LOCK_FILE = new File(CONFIG_DIR, "lock");
    private static final Lock JHBUILD_THREAD_LOCK = new ReentrantLock();
    private static final String LIB = "lib";
    private static final String LIB64 = "lib64";
    private static final String INCLUDE = "include";
    private static final String BIN = "bin";
    /**
     * The {@code git} binary to use.
     */
    /*
    internal implementation notes:
    - not final in order to allow overriding after installation
    */
    private String git;
    private String msgfmt;
    private String cpan;
    private String patch;
    private String openssl;
    /**
     * The {@code jhbuild} binary to use.
     */
    /*
    internal implementation notes:
    - not final in order to allow overriding after installation
    */
    private String jhbuild;
    private final String sh;
    private final String make;
    private String python3;
    /**
     * A C compiler is necessary to build Python in case
     * {@link #actionOnMissingPython3} is {@link ActionOnMissingBinary#DOWNLOAD}.
     */
    private final String cc;
    private String wget;
    private String pkgConfig;
    private String m4;
    private String xz;
    private final ActionOnMissingBinary actionOnMissingGit;
    private final ActionOnMissingBinary actionOnMissingZlib;
    private final ActionOnMissingBinary actionOnMissingJHBuild;
        //Mac OSX download is a .dmg download which can't be extracted locally
    private final ActionOnMissingBinary actionOnMissingPython3;
    private final ActionOnMissingBinary actionOnMissingMsgfmt;
    private final ActionOnMissingBinary actionOnMissingCpan;
    private final ActionOnMissingBinary actionOnMissingOpenssl;
    private final ActionOnMissingBinary actionOnMissingWget;
    private final ActionOnMissingBinary actionOnMissingPkgConfig;
    private final ActionOnMissingBinary actionOnMissingGnutls;
    private final ActionOnMissingBinary actionOnMissingNettle;
    private final ActionOnMissingBinary actionOnMissingXz;
    private final ActionOnMissingBinary actionOnMissingM4;
    private final ActionOnMissingBinary actionOnMissingGmp;
    private final boolean skipMD5SumCheck;
    private final File installationPrefixDir;
    private final File downloadDir;
    private boolean inited;
    /**
     * The {@link OutputStream} {@code stdout} of created processes ought to be
     * written to. {@code null} indicates that {@code stdout} of the JVM ought
     * to be used (exception and similar messages might contain the placeholder
     * {@code [redirected]} in this case).
     */
    private final OutputStream stdoutOutputStream;
    /**
     * The {@link OutputStream} {@code stderr} of created processes ought to be
     * written to. {@code null} indicates that {@code stderr} of the JVM ought
     * to be used (exception and similar messages might contain the placeholder
     * {@code [redirected]} in this case).
     */
    private final OutputStream stderrOutputStream;
    private boolean canceled;
    /**
     * A pointer to the currently active process which allows to destroy it in
     * {@link #cancelInstallModuleset() } and thus minimize the time before
     * returning after cancelation has been requested.
     */
    private Process activeProcess;
    private final Map<Process, Pair<OutputReaderThread, OutputReaderThread>> processOutputReaderThreadMap = new HashMap<>();
    private final Downloader downloader;
    /**
     * The value passed to the {@code -j} option of all invokations of
     * {@code make}, except {@code make install}.
     */
    private final int parallelism;
    /**
     * A reference to the output streams which are passed to each process to capture stdout and stderr in case of a
     * process failure in addition to the output streams passed to {@code JHBuildJavaWrapper} which are used by callers
     * and should not be interferred with.
     */
    private final Map<Process, Pair<ByteArrayOutputStream, ByteArrayOutputStream>> processErrorStreamMap = new HashMap<>();
    /**
     * How to present the output of {@code stdout} and {@code stderr} to the user in case of a failure. This is
     * unrelated to {@link #stdoutOutputStream} and {@link #stderrOutputStream}.
     */
    private final OutputMode outputMode;
    /**
     * Allows to limit the count of characters in process output in output mode "join".
     * @see #outputMode
     */
    /*
    internal implementation notes:
    - Setting the limit as long or in int representing KB doesn't make sense because most Java string-related data
    structures as well as the byte[] representation hold the data with max length 2^31, see
    https://stackoverflow.com/questions/3777819/java-string-substring-with-long-type-parameters for details.
     */
    private final int outputLimit;
    /**
     * Allows to control whether {@code jhbuild} exits immediately after any encountered error (using its
     * {@code --exit-on-error} option) or tries to build as much as possible.
     */
    private boolean exitOnError;

    public static int calculateParallelism() {
        return Runtime.getRuntime().availableProcessors()*2;
    }

    public JHBuildJavaWrapper(ActionOnMissingBinary actionOnMissingGit,
            ActionOnMissingBinary actionOnMissingZlib,
            ActionOnMissingBinary actionOnMissingJHBuild,
            ActionOnMissingBinary actionOnMissingPython3,
            ActionOnMissingBinary actionOnMissingMsgfmt,
            ActionOnMissingBinary actionOnMissingCpan,
            ActionOnMissingBinary actionOnMissingOpenssl,
            ActionOnMissingBinary actionOnMissingWget,
            ActionOnMissingBinary actionOnMissingPkgConfig,
            ActionOnMissingBinary actionOnMissingGnutls,
            ActionOnMissingBinary actionOnMissingNettle,
            ActionOnMissingBinary actionOnMissingXz,
            ActionOnMissingBinary actionOnMissingM4,
            ActionOnMissingBinary actionOnMissingGmp,
            Downloader downloader,
            boolean skipMD5SumCheck,
            OutputStream stdoutOutputStream,
            OutputStream stderrOutputStream,
            OutputMode outputMode,
            int outputLimit,
            boolean exitOnError) throws IOException {
        this(INSTALLATION_PREFIX_DIR_DEFAULT,
                DOWNLOAD_DIR_DEFAULT,
                actionOnMissingGit,
                actionOnMissingZlib,
                actionOnMissingJHBuild,
                actionOnMissingPython3,
                actionOnMissingMsgfmt,
                actionOnMissingCpan,
                actionOnMissingOpenssl,
                actionOnMissingWget,
                actionOnMissingPkgConfig,
                actionOnMissingGnutls,
                actionOnMissingNettle,
                actionOnMissingXz,
                actionOnMissingM4,
                actionOnMissingGmp,
                downloader,
                skipMD5SumCheck,
                stdoutOutputStream,
                stderrOutputStream,
                outputMode,
                outputLimit,
                exitOnError);
    }

    public JHBuildJavaWrapper(File installationPrefixDir,
            File downloadDir,
            ActionOnMissingBinary actionOnMissingGit,
            ActionOnMissingBinary actionOnMissingZlib,
            ActionOnMissingBinary actionOnMissingJHBuild,
            ActionOnMissingBinary actionOnMissingPython3,
            ActionOnMissingBinary actionOnMissingMsgfmt,
            ActionOnMissingBinary actionOnMissingCpan,
            ActionOnMissingBinary actionOnMissingOpenssl,
            ActionOnMissingBinary actionOnMissingWget,
            ActionOnMissingBinary actionOnMissingPkgConfig,
            ActionOnMissingBinary actionOnMissingGnutls,
            ActionOnMissingBinary actionOnMissingNettle,
            ActionOnMissingBinary actionOnMissingXz,
            ActionOnMissingBinary actionOnMissingM4,
            ActionOnMissingBinary actionOnMissingGmp,
            Downloader downloader,
            boolean skipMD5SumCheck,
            OutputStream stdoutOutputStream,
            OutputStream stderrOutputStream,
            OutputMode outputMode,
            int outputLimit,
            boolean exitOnError) throws IOException {
        this(installationPrefixDir,
                downloadDir,
                GIT_DEFAULT,
                JHBUILD_DEFAULT,
                SH_DEFAULT,
                MAKE_DEFAULT,
                PYTHON3_DEFAULT,
                CC_DEFAULT,
                MSGFMT_DEFAULT,
                CPAN_DEFAULT,
                PATCH_DEFAULT,
                OPENSSL_DEFAULT,
                WGET_DEFAULT,
                PKG_CONFIG_DEFAULT,
                XZ_DEFAULT,
                M4_DEFAULT,
                downloader,
                skipMD5SumCheck,
                stdoutOutputStream,
                stderrOutputStream,
                outputMode,
                outputLimit,
                exitOnError,
                actionOnMissingGit,
                actionOnMissingZlib,
                actionOnMissingJHBuild,
                actionOnMissingPython3,
                actionOnMissingMsgfmt,
                actionOnMissingCpan,
                actionOnMissingOpenssl,
                actionOnMissingWget,
                actionOnMissingPkgConfig,
                actionOnMissingGnutls,
                actionOnMissingNettle,
                actionOnMissingXz,
                actionOnMissingM4,
                actionOnMissingGmp,
                calculateParallelism());
    }

    public JHBuildJavaWrapper(File installationPrefixDir,
            File downloadDir,
            String git,
            String jhbuild,
            String sh,
            String make,
            String python3,
            String cc,
            String msgfmt,
            String cpan,
            String patch,
            String openssl,
            String wget,
            String pkgConfig,
            String xz,
            String m4,
            Downloader downloader,
            boolean skipMD5SumCheck,
            OutputStream stdoutOutputStream,
            OutputStream stderrOutputStream,
            OutputMode outputMode,
            int outputLimit,
            boolean exitOnError,
            ActionOnMissingBinary actionOnMissingGit,
            ActionOnMissingBinary actionOnMissingZlib,
            ActionOnMissingBinary actionOnMissingJHBuild,
            ActionOnMissingBinary actionOnMissingPython3,
            ActionOnMissingBinary actionOnMissingMsgfmt,
            ActionOnMissingBinary actionOnMissingCpan,
            ActionOnMissingBinary actionOnMissingOpenssl,
            ActionOnMissingBinary actionOnMissingWget,
            ActionOnMissingBinary actionOnMissingPkgConfig,
            ActionOnMissingBinary actionOnMissingGnutls,
            ActionOnMissingBinary actionOnMissingNettle,
            ActionOnMissingBinary actionOnMissingXz,
            ActionOnMissingBinary actionOnMissingM4,
            ActionOnMissingBinary actionOnMissingGmp,
            int parallelism) throws IOException {
        if(installationPrefixDir.exists() && !installationPrefixDir.isDirectory()) {
            throw new IllegalArgumentException("installationPrefixDir points "
                    + "to an existing location and is not a directory");
        }
        this.installationPrefixDir = installationPrefixDir;
        if(!installationPrefixDir.exists()) {
            FileUtils.forceMkdir(installationPrefixDir);
        }
        this.downloadDir = downloadDir;
        if(!downloadDir.exists()) {
            FileUtils.forceMkdir(downloadDir);
        }
        this.git = git;
        this.jhbuild = jhbuild;
        this.sh = sh;
        this.make = make;
        this.python3 = python3;
        this.cc = cc;
        this.msgfmt = msgfmt;
        this.cpan = cpan;
        this.patch = patch;
        this.openssl = openssl;
        this.wget = wget;
        this.pkgConfig = pkgConfig;
        this.xz = xz;
        this.m4 = m4;
        if(downloader == null) {
            throw new IllegalArgumentException("downloader mustn't be null");
        }
        this.downloader = downloader;
        this.actionOnMissingGit = actionOnMissingGit;
        this.actionOnMissingZlib = actionOnMissingZlib;
        this.actionOnMissingJHBuild = actionOnMissingJHBuild;
        this.actionOnMissingPython3 = actionOnMissingPython3;
        this.actionOnMissingMsgfmt = actionOnMissingMsgfmt;
        this.actionOnMissingCpan = actionOnMissingCpan;
        this.actionOnMissingOpenssl = actionOnMissingOpenssl;
        this.actionOnMissingWget = actionOnMissingWget;
        this.actionOnMissingPkgConfig = actionOnMissingPkgConfig;
        this.actionOnMissingGnutls = actionOnMissingGnutls;
        this.actionOnMissingNettle = actionOnMissingNettle;
        this.actionOnMissingXz = actionOnMissingXz;
        this.actionOnMissingM4 = actionOnMissingM4;
        this.actionOnMissingGmp = actionOnMissingGmp;
        this.skipMD5SumCheck = skipMD5SumCheck;
        this.stdoutOutputStream = stdoutOutputStream;
        this.stderrOutputStream = stderrOutputStream;
        this.outputMode = outputMode;
        this.outputLimit = outputLimit;
        this.exitOnError = exitOnError;
        if(parallelism < 1) {
            throw new IllegalArgumentException(String.format("parallelism value of less than 1 doesn't make sense (was %d)",
                    parallelism));
        }
        this.parallelism = parallelism;
    }

    public File getDownloadDir() {
        return downloadDir;
    }

    public File getInstallationPrefixDir() {
        return installationPrefixDir;
    }

    private Process createProcess(String path,
            String... commands) throws IOException {
        return createProcess(null,
                path,
                commands);
    }

    private Process createProcess(File directory,
            String path,
            String... commands) throws IOException {
        return createProcess(directory,
                ImmutableMap.<String, String>builder()
                        .put("PATH", path)
                        .build(),
                commands);
    }

    /*
    internal implementation notes:
    - checking for canceled doesn't make sense here because null would have to
    be returned or an exception thrown in the case of canceled state which
    creates the need to evaluate the return value by callers which is equally
    complex as checking the condition before calls to createProcess
    */
    /**
     * Allows sharing code between different process creation routines.
     *
     * @param directory the working directory
     * @param env the environment
     * @param commands the command string(s)
     * @return the created process
     * @throws IOException if an I/O exception occurs during creation
     */
    private Process createProcess(File directory,
            Map<String, String> env,
            String... commands) throws IOException {
        LOGGER.debug(String.format("building process with commands '%s' ('%s') with environment '%s' running in %s",
                Arrays.toString(commands),
                String.join(" ", commands),
                env,
                directory != null ? String.format("directory '%s'",
                        directory.getAbsolutePath())
                        : "current directory"));
        ByteArrayOutputStream stdoutErrorStream = new ByteArrayOutputStream();
        ByteArrayOutputStream stderrErrorStream = new ByteArrayOutputStream();
            //see Javadoc for processErrorStreamMap
        ByteArrayOutputStream stderrErrorStream0;
        if(outputMode == OutputMode.JOIN_STDOUT_STDERR) {
            stderrErrorStream0 = stdoutErrorStream;
        }else if(outputMode == OutputMode.SPLIT_STDOUT_STDERR) {
            stderrErrorStream0 = stderrErrorStream;
        }else {
            throw new IllegalArgumentException(String.format(OUTPUT_MODE_TEMPLATE,
                    outputMode));
        }
        Triple<Process, OutputReaderThread, OutputReaderThread> process = ExecutionUtils.createProcess(directory,
                env,
                sh,
                new LinkedList<>(Arrays.asList(stdoutOutputStream, stdoutErrorStream)),
                new LinkedList<>(Arrays.asList(stderrOutputStream, stderrErrorStream0)),
                OutputReaderThreadMode.OUTPUT_STREAM,
                commands);
        processErrorStreamMap.put(process.getLeft(),
                new ImmutablePair<>(stdoutErrorStream, stderrErrorStream0));
        processOutputReaderThreadMap.put(process.getLeft(),
                new ImmutablePair<>(process.getMiddle(),
                        process.getRight()));
        synchronized(this) {
            this.activeProcess = process.getLeft();
        }
        return process.getLeft();
    }

    /**
     * Initialization routines.
     *
     * @param installationPrefixPath the installation prefix path
     * @return {@code false} if the download or any build step has been
     *     canceled, {@code true} otherwise
     * @throws IOException if such an exception occurs
     * @throws ExtractionException if such an exception occurs
     * @throws InterruptedException if such an exception occurs
     * @throws MissingSystemBinaryException if such an exception occurs
     * @throws BuildFailureException if such an exception occurs
     */
    @SuppressWarnings({"PMD.TooFewBranchesForASwitchStatement",
        "PMD.PreserveStackTrace"})
    private boolean init(String installationPrefixPath) throws IOException,
            ExtractionException,
            InterruptedException,
            MissingSystemBinaryException,
            BuildFailureException,
            DownloadException {
        if(inited) {
            LOGGER.debug("already inited");
            return true;
        }
        assert downloadDir.exists() && downloadDir.isDirectory();
        LOGGER.trace(String.format("silenceStdout: %s",
                stdoutOutputStream));
        LOGGER.trace(String.format("silenceStderr: %s",
                stderrOutputStream));
        try {
            BinaryUtils.validateBinary(cc,
                    "cc",
                    installationPrefixPath);
        }catch(BinaryValidationException gccBinaryValidationException) {
            //there's no sense in providing options/MissingBiaryAction since
            //building GCC from source without a working C compiler is between
            //troublesome and impossible
            throw new MissingSystemBinaryException("cc",
                    gccBinaryValidationException);
        }
        //need make for building and it's overly hard to bootstrap
        //without it, so force installation out of JHBuild wrapper
        try {
            BinaryUtils.validateBinary(make,
                    MAKE_BINARY_NAME,
                    installationPrefixPath);
        }catch(BinaryValidationException ex1) {
            throw new MissingSystemBinaryException(MAKE_BINARY_NAME,
                    ex1);
        }
        //git needs `Module::Build` which needs to be installed with `cpan`
        //which is provided by a complete Perl installation only
        try {
            BinaryUtils.validateBinary(cpan,
                    CPAN_TEMPLATE,
                    installationPrefixPath);
            LOGGER.debug("Using existing cpan binary '{}'",
                    cpan);
        }catch(BinaryValidationException ex) {
            LOGGER.debug("cpan binary '{}' not found or not valid, taking action {}",
                    cpan,
                    actionOnMissingCpan);
            switch(actionOnMissingCpan) {
                case FAIL:
                    throw new IllegalStateException(String.format("cpan binary '%s' doesn't exist and can't be found in PATH",
                            cpan),
                            ex);
                case DOWNLOAD:
                    DownloadCombi perlDownloadCombi = new DownloadCombi("http://www.cpan.org/src/5.0/perl-5.26.1.tar.gz",
                            new File(downloadDir,
                                    "perl-5.26.1.tar.gz").getAbsolutePath(),
                            ExtractionMode.EXTRACTION_MODE_TAR_GZ,
                            new File(downloadDir,
                                    "perl-5.26.1").getAbsolutePath(),
                            "a7e5c531ee1719c53ec086656582ea86");
                    List<BuildStepProcess> buildStepProcesses = generateBuildStepProcessesAutotools(installationPrefixPath,
                            parallelism,
                            "configure.gnu");
                    cpan = installPrerequisiteAutotools(installationPrefixPath,
                            CPAN_TEMPLATE,
                            CPAN_TEMPLATE,
                            perlDownloadCombi,
                            null, //patchDownloadCombis
                            buildStepProcesses
                    );
                    if(cpan == null) {
                        //interactive download has been canceled
                        return false;
                    }
                    try {
                        BinaryUtils.validateBinary(cpan,
                                CPAN_TEMPLATE,
                                installationPrefixPath);
                    } catch (BinaryValidationException binaryValidationExceptionAfterInstallation) {
                        throw new IllegalStateException("cpan exisistence check or installation failed when cpan " +
                                "installation is expected to have been successful",
                                binaryValidationExceptionAfterInstallation);
                    }
                    break;
                default:
                    throw new IllegalArgumentException(String.format(ACTION_TEMPLATE,
                            actionOnMissingCpan));
            }
        }
        //gettext is a prerequisite of git (needs `msgfmt` command which is in
        //`gettext-tools`)
        try {
            BinaryUtils.validateBinary(msgfmt,
                    "gettext",
                    installationPrefixPath);
            LOGGER.debug("Using existing msgfmt binary '{}'",
                    msgfmt);
        }catch(BinaryValidationException ex) {
            LOGGER.debug("msgfmt binary '{}' not found or not valid, taking action {}",
                    msgfmt,
                    actionOnMissingMsgfmt);
            switch(actionOnMissingMsgfmt) {
                case FAIL:
                    throw new IllegalStateException(String.format("msgfmt binary '%s' doesn't exist and can't be found in PATH",
                            msgfmt),
                            ex);
                case DOWNLOAD:
                    DownloadCombi gettextDownloadCombi = new DownloadCombi("https://ftp.gnu.org/pub/gnu/gettext/gettext-0.19.8.1.tar.gz",
                            new File(downloadDir,
                                    "gettext-0.19.8.1.tar.gz").getAbsolutePath(),
                            ExtractionMode.EXTRACTION_MODE_TAR_GZ,
                            new File(downloadDir,
                                    "gettext-0.19.8.1").getAbsolutePath(),
                            "97e034cf8ce5ba73a28ff6c3c0638092");
                    DownloadCombi gettextPatchDownloadCombi = new DownloadCombi(JHBuildJavaWrapper.class.getResource("/patches/gettext/texi2html.patch").toExternalForm(),
                            new File(downloadDir,
                                    "texi2html.patch").getAbsolutePath(),
                            ExtractionMode.EXTRACTION_MODE_NONE,
                            new File(downloadDir,
                                    "texi2html.patch").getAbsolutePath(),
                            "77c7ac38a7cacab88753da0f0d8936fb");
                    msgfmt = installPrerequisiteAutotools(installationPrefixPath,
                            "msgfmt",
                            "gettext",
                            gettextDownloadCombi,
                            new LinkedList<>(Arrays.asList(gettextPatchDownloadCombi)), //patchDownloadCombis
                            parallelism);
                    if(msgfmt == null) {
                        //interactive download has been canceled
                        return false;
                    }
                    try {
                        BinaryUtils.validateBinary(msgfmt,
                                "msgfmt",
                                installationPrefixPath);
                    } catch (BinaryValidationException binaryValidationExceptionAfterInstallation) {
                        throw new IllegalArgumentException("msgfmt exisistence check or installation failed when " +
                                "gettext installation is expected to have been successful",
                                binaryValidationExceptionAfterInstallation);
                    }
                    break;
                default:
                    throw new IllegalArgumentException(String.format(ACTION_TEMPLATE,
                            actionOnMissingMsgfmt));
            }
        }
        //zlib is a prerequisite of git and python3 build
        try {
            Path existingZlibPc = checkLibPresence(installationPrefixDir,
                    "zlib.pc");
            LOGGER.debug(String.format("using existing version of zlib with pkg-config file '%s'",
                    existingZlibPc.toFile().getAbsolutePath()));
        }catch(LibraryNotPresentException ex) {
            LOGGER.debug("zlib not found or not valid, taking action {}",
                    actionOnMissingZlib);
            switch(actionOnMissingZlib) {
                case FAIL:
                    throw new IllegalStateException("library zlib doesn't exist in installation prefix or on the system",
                            ex);
                case DOWNLOAD:
                    DownloadCombi zlibDownloadCombi = new DownloadCombi("https://www.zlib.net/zlib-1.2.11.tar.gz", //downloadURL
                            new File(downloadDir,
                                    "zlib-1.2.11.tar.gz").getAbsolutePath(), //downloadTarget
                            ExtractionMode.EXTRACTION_MODE_TAR_GZ,
                            new File(downloadDir,
                                    "zlib-1.2.11").getAbsolutePath(), //extractionLocation
                            "1c9f62f0778697a09d36121ead88e08e" //md5sum
                    );
                    String zlib = installPrerequisiteAutotools(installationPrefixPath,
                            "", //binary (library doesn't provide binary, see
                                //installPrerequisiteAutotools for details)
                            "zlib",
                            zlibDownloadCombi,
                            null, //patchDownloadCombi,
                            parallelism);
                    if(zlib == null) {
                        //interactive download has been canceled
                        return false;
                    }
                    assert "".equals(zlib);
                    break;
                default:
                    throw new IllegalArgumentException(String.format(ACTION_TEMPLATE,
                            actionOnMissingZlib));
            }
        }
        try {
            BinaryUtils.validateBinary(git,
                    GIT_TEMPLATE,
                    installationPrefixPath);
            LOGGER.debug("Using existing git binary '{}'",
                    git);
        }catch(BinaryValidationException ex) {
            LOGGER.debug("git binary '{}' not found or not valid, taking action {}",
                    git,
                    actionOnMissingGit);
            switch(actionOnMissingGit) {
                case FAIL:
                    throw new IllegalStateException(String.format("git binary '%s' doesn't exist and can't be found in PATH",
                            git),
                            ex);
                case DOWNLOAD:
                    DownloadCombi gitDownloadCombi = new DownloadCombi("https://www.kernel.org/pub/software/scm/git/git-2.13.3.tar.gz",
                            new File(downloadDir,
                                    "git-2.13.3.tar.gz").getAbsolutePath(),
                            ExtractionMode.EXTRACTION_MODE_TAR_GZ,
                            new File(downloadDir,
                                    "git-2.13.3").getAbsolutePath(),
                            "d2dc550f6693ba7e5b16212b2714f59f");
                    git = installPrerequisiteAutotools(installationPrefixPath,
                            GIT_TEMPLATE,
                            GIT_TEMPLATE,
                            gitDownloadCombi,
                            null, //patchDownloadCombi,
                            parallelism);
                    if(git == null) {
                        //interactive download has been canceled
                        return false;
                    }
                    try {
                        BinaryUtils.validateBinary(git,
                                GIT_TEMPLATE,
                                installationPrefixPath);
                    } catch (BinaryValidationException binaryValidationExceptionAfterInstallation) {
                        throw new IllegalArgumentException("git exisistence check or installation failed when git " +
                                "installation is expected to have been successful",
                                binaryValidationExceptionAfterInstallation);
                    }
                    break;
                default:
                    throw new IllegalArgumentException(String.format(ACTION_TEMPLATE,
                            actionOnMissingGit));
            }
        }
        try {
            Path existingOpensslPc = checkLibPresence(installationPrefixDir,
                    "openssl.pc");
                // checking for presence and validity of the openssl binary isn't sufficient because in a rather empty
                // container the binary might be available without the headers and shared object files
            LOGGER.debug("using existing version of openssl with pkg-config file '%s'",
                    existingOpensslPc.toFile().getAbsolutePath());
        }catch(LibraryNotPresentException ex) {
            LOGGER.debug("openssl not found or not valid, taking action {}",
                    actionOnMissingOpenssl);
            switch(actionOnMissingOpenssl) {
                case FAIL:
                    throw new IllegalStateException("library openssl doesn't exist in installation prefix or on the system",
                            ex);
                case DOWNLOAD:
                    DownloadCombi opensslDownloadCombi = new DownloadCombi("https://www.openssl.org/source/openssl-1.1.1f.tar.gz",
                            new File(downloadDir,
                                    "openssl-1.1.1f.tar.gz").getAbsolutePath(),
                            ExtractionMode.EXTRACTION_MODE_TAR_GZ,
                            new File(downloadDir,
                                    "openssl-1.1.1f").getAbsolutePath(),
                            "3f486f2f4435ef14b81814dbbc7b48bb");
                    List<BuildStepProcess> buildStepProcesses = generateBuildStepProcessesAutotools(installationPrefixPath,
                            parallelism,
                            "config");
                    openssl = installPrerequisiteAutotools(installationPrefixPath,
                            OPENSSL_TEMPLATE,
                            OPENSSL_TEMPLATE,
                            opensslDownloadCombi,
                            null, //patchDownloadCombis
                            buildStepProcesses
                    );
                    if(openssl == null) {
                        //interactive download has been canceled
                        return false;
                    }
                    try {
                        BinaryUtils.validateBinary(openssl,
                                OPENSSL_TEMPLATE,
                                installationPrefixPath);
                    } catch (BinaryValidationException binaryValidationExceptionAfterInstallation) {
                        throw new IllegalArgumentException("openssl exisistence check or installation failed when " +
                                "openssl installation is expected to have been successful",
                                binaryValidationExceptionAfterInstallation);
                    }
                    break;
                default:
                    throw new IllegalArgumentException(String.format(ACTION_TEMPLATE,
                            actionOnMissingOpenssl));
            }
        }
        // pkg-config
        // - is a prerequisite of wget
        // - might be a prerequisite for Python 2.x detection of OpenSSL installation in an arbitrary prefix specified
        // with SSL make variable (definitely not possible without specifying the variable); check as soon as this order
        // causes trouble
        try {
            BinaryUtils.validateBinary(pkgConfig,
                    PKG_CONFIG_TEMPLATE,
                    installationPrefixPath);
            LOGGER.debug("Using existing pkg-config binary '{}'",
                    pkgConfig);
        }catch(BinaryValidationException ex) {
            LOGGER.debug("pkg-config binary '{}' not found or not valid, taking action {}",
                    pkgConfig,
                    actionOnMissingPkgConfig);
            switch(actionOnMissingPkgConfig) {
                case FAIL:
                    throw new IllegalStateException(String.format("pkg-config binary '%s' doesn't exist and can't be found in PATH",
                            pkgConfig),
                            ex);
                case DOWNLOAD:
                    DownloadCombi pkgConfigDownloadCombi = new DownloadCombi("https://pkgconfig.freedesktop.org/releases/pkg-config-0.29.2.tar.gz",
                            new File(downloadDir,
                                    "pkg-config-0.29.2.tar.gz").getAbsolutePath(),
                            ExtractionMode.EXTRACTION_MODE_TAR_GZ,
                            new File(downloadDir,
                                    "pkg-config-0.29.2").getAbsolutePath(),
                            "f6e931e319531b736fadc017f470e68a");
                    pkgConfig = installPrerequisiteAutotools(installationPrefixPath,
                            PKG_CONFIG_TEMPLATE,
                            PKG_CONFIG_TEMPLATE,
                            pkgConfigDownloadCombi,
                            null, //patchDownloadCombi,
                            parallelism,
                            new String[] {"--with-internal-glib"},
                            null, //additionalMakeVariables
                            Collections.emptyMap()
                    );
                    if(pkgConfig == null) {
                        //interactive download has been canceled
                        return false;
                    }
                    try {
                        BinaryUtils.validateBinary(pkgConfig,
                                PKG_CONFIG_TEMPLATE,
                                installationPrefixPath);
                    } catch (BinaryValidationException binaryValidationExceptionAfterInstallation) {
                        throw new IllegalArgumentException("pkg-config exisistence check or installation failed when " +
                                "pkg-config installation is expected to have been successful",
                                binaryValidationExceptionAfterInstallation);
                    }
                    break;
                default:
                    throw new IllegalArgumentException(String.format(ACTION_TEMPLATE,
                            actionOnMissingPkgConfig));
            }
        }
        //unclear why git version of Python 2.x has been used before (only increases
        //download time and might include instabilities from master)
        try {
            BinaryUtils.validateBinary(python3,
                    PYTHON3_TEMPLATE,
                    installationPrefixPath);
            LOGGER.debug("Using existing python3 binary '{}'",
                    python3);
        }catch(BinaryValidationException ex1) {
            LOGGER.debug("python3 binary '{}' not found or not valid, taking action {}",
                    python3,
                    actionOnMissingPython3);
            switch(actionOnMissingPython3) {
                case FAIL:
                    throw new IllegalStateException(String.format("python3 binary '%s' doesn't exist and can't be found in PATH",
                            python3),
                            ex1);
                case DOWNLOAD:
                    DownloadCombi python3DownloadCombi = new DownloadCombi("https://www.python.org/ftp/python/3.8.2/Python-3.8.2.tgz",
                            new File(downloadDir,
                                    "Python-3.8.2.tgz").getAbsolutePath(),
                            ExtractionMode.EXTRACTION_MODE_TAR_GZ,
                            new File(downloadDir,
                                    "Python-3.8.2").getAbsolutePath(),
                            "f9f3768f757e34b342dbc06b41cbc844");
                    python3 = installPrerequisiteAutotools(installationPrefixPath,
                            PYTHON3_TEMPLATE,
                            PYTHON3_TEMPLATE,
                            python3DownloadCombi,
                            null, //patchDownloadCombi
                            parallelism,
                            null, //additionalConfigureOptions
                            new String[] {String.format("SSL=%s", installationPrefixPath)}, //additionalMakeVariables
                            Collections.emptyMap()
                    );
                    if(python3 == null) {
                        //interactive download has been canceled
                        return false;
                    }
                    try {
                        BinaryUtils.validateBinary(python3,
                                PYTHON3_TEMPLATE,
                                installationPrefixPath);
                    } catch (BinaryValidationException binaryValidationExceptionAfterInstallation) {
                        throw new IllegalArgumentException("python3 exisistence check or installation failed when " +
                                "python3 installation is expected to have been successful",
                                binaryValidationExceptionAfterInstallation);
                    }
                    python3PostInstallation();
                    break;
                default:
                    throw new IllegalArgumentException(String.format(ACTION_TEMPLATE,
                            actionOnMissingPython3));
            }
        }
        try {
            BinaryUtils.validateBinary(jhbuild,
                    JHBUILD_TEMPLATE,
                    installationPrefixPath);
            LOGGER.debug("Using existing jhbuild binary '{}'",
                    jhbuild);
        }catch(BinaryValidationException ex) {
            LOGGER.debug("jhbuild binary '{}' not found or not valid, taking action {}",
                    jhbuild,
                    actionOnMissingJHBuild);
            switch(actionOnMissingJHBuild) {
                case FAIL:
                    throw new IllegalStateException(String.format("jhbuild binary '%s' doesn't exist and can't be found in PATH",
                            jhbuild),
                            ex);
                case DOWNLOAD:
                    if(!jhbuildDownload(installationPrefixPath)) {
                        return false;
                    }
                    break;
                default:
                    throw new IllegalArgumentException(String.format(ACTION_TEMPLATE,
                            actionOnMissingJHBuild));
            }
        }
        //m4 is prerequisite of nettle
        try {
            BinaryUtils.validateBinary(m4,
                    M4_TEMPLATE,
                    installationPrefixPath);
            LOGGER.debug("Using existing m4 binary '{}'",
                    m4);
        }catch(BinaryValidationException ex) {
            if(m4BuildFailureOnCurrentOS()) {
                throw new IllegalStateException("m4 isn't available and can't be built on the current OS (see " +
                        "https://askubuntu.com/questions/1099392/compilation-of-m4-1-4-10-to-1-4-18-fails-due-to-please-port-gnulib-freadahead-c " +
                        "for details)",
                        ex);
            }
            LOGGER.debug("m4 binary '{}' not found or not valid, taking action {}",
                    m4,
                    actionOnMissingM4);
            switch(actionOnMissingM4) {
                case FAIL:
                    throw new IllegalStateException(String.format("m4 binary '%s' doesn't exist and can't be found in PATH",
                            m4),
                            ex);
                case DOWNLOAD:
                    DownloadCombi m4DownloadCombi = new DownloadCombi("https://ftp.gnu.org/gnu/m4/m4-1.4.9.tar.gz",
                            new File(downloadDir,
                                    "m4-1.4.9.tar.gz").getAbsolutePath(),
                            ExtractionMode.EXTRACTION_MODE_TAR_GZ,
                            new File(downloadDir,
                                    "m4-1.4.9").getAbsolutePath(),
                            "1ba8e147aff5e79bd2bfb983d86b53d5");
                        // the latest m4 version not exhibiting
                        // `freadahead.c:92:3: error: #error "Please port gnulib freadahead.c to your platform! Look at the definition of fflush, fread, ungetc on your system, then report this to bug-gnulib."`
                        // on Ubuntu 18.10 is 1.4.9. 1.4.18 still exhibits the bug on Ubuntu 19.04 and 20.04.
                        //
                        // The bug has been reported probably more than 5 times on the
                        // mailing list (searching, sorting and marking bugs as duplicate on a mailing list is nonsense
                        // as it's not a tool for collaboration on software development, therefore initialized
                        // engagement in testing and building the FSF forge). Until FSF and GNU are providing means to
                        // develop software, use a preinstalled version of m4
                    m4 = installPrerequisiteAutotools(installationPrefixPath,
                            M4_TEMPLATE,
                            M4_TEMPLATE,
                            m4DownloadCombi,
                            null, //patchDownloadCombi,
                            parallelism);
                    if(m4 == null) {
                        //interactive download has been canceled
                        return false;
                    }
                    try {
                        BinaryUtils.validateBinary(m4,
                                M4_TEMPLATE,
                                installationPrefixPath);
                    } catch (BinaryValidationException binaryValidationExceptionAfterInstallation) {
                        throw new IllegalArgumentException("m4 exisistence check or installation failed when " +
                                "m4 installation is expected to have been successful",
                                binaryValidationExceptionAfterInstallation);
                    }
                    break;
                default:
                    throw new IllegalArgumentException(String.format(ACTION_TEMPLATE,
                            actionOnMissingM4));
            }
        }
        //gmp is a prerequisite of nettle with gmp
        try {
            Path existingGmpPc = checkLibPresence(installationPrefixDir,
                    "gmp.pc");
            LOGGER.debug(String.format("using existing version of gmp with pkg-config file '%s'",
                    existingGmpPc.toFile().getAbsolutePath()));
        }catch(LibraryNotPresentException ex) {
            LOGGER.debug("gmp not found or not valid, taking action {}",
                    actionOnMissingGmp);
            switch(actionOnMissingGmp) {
                case FAIL:
                    throw new IllegalStateException("library gmp doesn't exist in installation prefix or on the system",
                            ex);
                case DOWNLOAD:
                    DownloadCombi gmpDownloadCombi = new DownloadCombi("https://gmplib.org/download/gmp/gmp-6.2.0.tar.gz",
                            new File(downloadDir,
                                    "gmp-6.2.0.tar.gz").getAbsolutePath(),
                            ExtractionMode.EXTRACTION_MODE_TAR_GZ,
                            new File(downloadDir,
                                    "gmp-6.2.0").getAbsolutePath(),
                            "68bc9b1119a316fdc49938e2d63e942e");
                    String gmp = installPrerequisiteAutotools(installationPrefixPath,
                            "", //binary (library doesn't provide binary, see
                                //installPrerequisiteAutotools for details)
                            GMP_TEMPLATE,
                            gmpDownloadCombi,
                            null, //patchDownloadCombi,
                            parallelism,
                            new File(gmpDownloadCombi.getExtractionLocation(), CONFIGURE).getAbsolutePath());
                    if(gmp == null) {
                        //interactive download has been canceled
                        return false;
                    }
                    assert "".equals(gmp);
                    break;
                default:
                    throw new IllegalArgumentException(String.format(ACTION_TEMPLATE,
                            actionOnMissingGmp));
            }
        }
        //nettle with gmp is a prerequisite of gnutls
        try {
            Path existingNettlePc = checkLibPresence(installationPrefixDir,
                    "nettle.pc");
            LOGGER.debug(String.format("using existing version of nettle with pkg-config file '%s'",
                    existingNettlePc.toFile().getAbsolutePath()));
        }catch(LibraryNotPresentException ex) {
            LOGGER.debug("nettle not found or not valid, taking action {}",
                    actionOnMissingNettle);
            switch(actionOnMissingNettle) {
                case FAIL:
                    throw new IllegalStateException("library nettle doesn't exist in installation prefix or on the system",
                            ex);
                case DOWNLOAD:
                    DownloadCombi nettleDownloadCombi = new DownloadCombi("https://ftp.gnu.org/gnu/nettle/nettle-3.5.1.tar.gz",
                            new File(downloadDir,
                                    "nettle-3.5.1.tar.gz").getAbsolutePath(),
                            ExtractionMode.EXTRACTION_MODE_TAR_GZ,
                            new File(downloadDir,
                                    "nettle-3.5.1").getAbsolutePath(),
                            "0e5707b418c3826768d41130fbe4ee86");
                    String nettle = installPrerequisiteAutotools(installationPrefixPath,
                            "", //binary (library doesn't provide binary, see
                                //installPrerequisiteAutotools for details)
                            NETTLE_TEMPLATE,
                            nettleDownloadCombi,
                            null, //patchDownloadCombi,
                            parallelism,
                            new File(nettleDownloadCombi.getExtractionLocation(), CONFIGURE).getAbsolutePath());
                    if(nettle == null) {
                        //interactive download has been canceled
                        return false;
                    }
                    assert "".equals(nettle);
                    break;
                default:
                    throw new IllegalArgumentException(String.format(ACTION_TEMPLATE,
                            actionOnMissingNettle));
            }
        }
        // xz is necessary as long as gnutls doesn't provide gzipped tarballs (suggested at https://gitlab.com/gnutls/gnutls/-/issues/969)
        try {
            Path existingLiblzmaPc = checkLibPresence(installationPrefixDir,
                    "liblzma.pc");
            LOGGER.debug(String.format("using existing version of xz with pkg-config file '%s'",
                    existingLiblzmaPc.toFile().getAbsolutePath()));
        }catch(LibraryNotPresentException ex) {
            LOGGER.debug("xz not found or not valid, taking action {}",
                    actionOnMissingXz);
            switch(actionOnMissingXz) {
                case FAIL:
                    throw new IllegalStateException("library xz doesn't exist in installation prefix or on the system",
                            ex);
                case DOWNLOAD:
                    DownloadCombi xzDownloadCombi = new DownloadCombi("https://tukaani.org/xz/xz-5.2.5.tar.gz",
                            new File(downloadDir,
                                    "xz-5.2.5.tar.gz").getAbsolutePath(),
                            ExtractionMode.EXTRACTION_MODE_TAR_GZ,
                            new File(downloadDir,
                                    "xz-5.2.5").getAbsolutePath(),
                            "0d270c997aff29708c74d53f599ef717");
                    xz = installPrerequisiteAutotools(installationPrefixPath,
                            "xz",
                            XZ_TEMPLATE,
                            xzDownloadCombi,
                            null, //patchDownloadCombi,
                            parallelism,
                            new File(xzDownloadCombi.getExtractionLocation(), CONFIGURE).getAbsolutePath());
                    if(xz == null) {
                        //interactive download has been canceled
                        return false;
                    }
                    try {
                        BinaryUtils.validateBinary(xz,
                                XZ_TEMPLATE,
                                installationPrefixPath);
                    } catch (BinaryValidationException binaryValidationExceptionAfterInstallation) {
                        throw new IllegalArgumentException("xz exisistence check or installation failed when " +
                                "xz installation is expected to have been successful",
                                binaryValidationExceptionAfterInstallation);
                    }
                    break;
                default:
                    throw new IllegalArgumentException(String.format(ACTION_TEMPLATE,
                            actionOnMissingXz));
            }
        }
        //gnutls is a prerequisite of wget
        try {
            Path existingGnutlsPc = checkLibPresence(installationPrefixDir,
                    "gnutls.pc");
            LOGGER.debug(String.format("using existing version of gnutls with pkg-config file '%s'",
                    existingGnutlsPc.toFile().getAbsolutePath()));
        }catch(LibraryNotPresentException ex) {
            LOGGER.debug("gnutls not found or not valid, taking action {}",
                    actionOnMissingGnutls);
            switch(actionOnMissingGnutls) {
                case FAIL:
                    throw new IllegalStateException("library gnutls doesn't exist in installation prefix or on the system",
                            ex);
                case DOWNLOAD:
                    DownloadCombi gnutlsDownloadCombi = new DownloadCombi("https://www.gnupg.org/ftp/gcrypt/gnutls/v3.6/gnutls-3.6.13.tar.xz",
                            new File(downloadDir,
                                    "gnutls-3.6.13.tar.xz").getAbsolutePath(),
                            ExtractionMode.EXTRACTION_MODE_TAR_XZ,
                            new File(downloadDir,
                                    "gnutls-3.6.13").getAbsolutePath(),
                            "bb1fe696a11543433785b4fc70ca225f");
                    String gnutls = installPrerequisiteAutotools(installationPrefixPath,
                            "", //binary (library doesn't provide binary, see
                                //installPrerequisiteAutotools for details)
                            GNUTLS_TEMPLATE,
                            gnutlsDownloadCombi,
                            null, //patchDownloadCombi,
                            parallelism,
                            new File(gnutlsDownloadCombi.getExtractionLocation(), CONFIGURE).getAbsolutePath(),
                            new String[] {"--with-included-libtasn1",
                                "--with-included-unistring",
                                "--without-p11-kit"},
                            null, //additionalMakeVariables
                            ubuntu1910PkgConfigPath()
                    );
                    if(gnutls == null) {
                        //interactive download has been canceled
                        return false;
                    }
                    assert "".equals(gnutls);
                    break;
                default:
                    throw new IllegalArgumentException(String.format(ACTION_TEMPLATE,
                            actionOnMissingGnutls));
            }
        }
        try {
            BinaryUtils.validateBinary(wget,
                    WGET_TEMPLATE,
                    installationPrefixPath);
            LOGGER.debug("Using existing wget binary '{}'",
                    wget);
        }catch(BinaryValidationException ex) {
            LOGGER.debug("wget binary '{}' not found or not valid, taking action {}",
                    wget,
                    actionOnMissingWget);
            switch(actionOnMissingWget) {
                case FAIL:
                    throw new IllegalStateException(String.format("wget binary '%s' doesn't exist and can't be found in PATH",
                            wget),
                            ex);
                case DOWNLOAD:
                    DownloadCombi wgetDownloadCombi = new DownloadCombi("https://ftp.gnu.org/gnu/wget/wget-1.20.3.tar.gz",
                            new File(downloadDir,
                                    "wget-1.20.3.tar.gz").getAbsolutePath(),
                            ExtractionMode.EXTRACTION_MODE_TAR_GZ,
                            new File(downloadDir,
                                    "wget-1.20.3").getAbsolutePath(),
                            "db4e6dc7977cbddcd543b240079a4899");
                    wget = installPrerequisiteAutotools(installationPrefixPath,
                            WGET_TEMPLATE,
                            WGET_TEMPLATE,
                            wgetDownloadCombi,
                            null, //patchDownloadCombi,
                            parallelism,
                            new String[] {String.format("--with-libgnutls-prefix=%s", installationPrefixDir.getAbsolutePath())},
                            null,
                            ubuntu1910PkgConfigPath());
                    if(wget == null) {
                        //interactive download has been canceled
                        return false;
                    }
                    try {
                        BinaryUtils.validateBinary(wget,
                                WGET_TEMPLATE,
                                installationPrefixPath);
                    } catch (BinaryValidationException binaryValidationExceptionAfterInstallation) {
                        throw new IllegalArgumentException("wget exisistence check or installation failed when " +
                                "wget installation is expected to have been successful",
                                binaryValidationExceptionAfterInstallation);
                    }
                    break;
                default:
                    throw new IllegalArgumentException(String.format(ACTION_TEMPLATE,
                            actionOnMissingWget));
            }
        }
        this.inited = true;
        return true;
    }

    public static boolean m4BuildFailureOnCurrentOS() throws IOException {
        UbuntuVersion ubuntuVersion = DownloadUtils.getUbuntuVersion();
        if (ubuntuVersion != null
                && (ubuntuVersion.getMajor() == 18 && ubuntuVersion.getMinor() == 10
                || ubuntuVersion.getMajor() == 19 && ubuntuVersion.getMinor() == 4
                || ubuntuVersion.getMajor() == 19 && ubuntuVersion.getMinor() == 10
                || ubuntuVersion.getMajor() == 20 && ubuntuVersion.getMinor() == 4)) {
            return true;
        }
        DebianVersion debianVersion = DownloadUtils.getDebianVersion();
        return debianVersion != null && debianVersion.getMajor() == 10;
    }

    private Map<String, String> ubuntu1910PkgConfigPath() {
        return ImmutableMap.of("PKG_CONFIG_PATH", String.format("%s:%s",
                Paths.get(installationPrefixDir.getAbsolutePath(), LIB64, "pkgconfig").toString(),
                Paths.get(installationPrefixDir.getAbsolutePath(), LIB, "pkgconfig").toString()));
    }

    private void python3PostInstallation() throws IOException {
        final Path python3ShortcutBinary = Paths.get(installationPrefixDir.getAbsolutePath(), BIN, "python3");
        final Path pip3ShortcutBinary = Paths.get(installationPrefixDir.getAbsolutePath(), BIN, "pip3");
        if (!python3ShortcutBinary.toFile().exists()) {
            Files.createSymbolicLink(python3ShortcutBinary,
                    Paths.get(installationPrefixDir.getAbsolutePath(), BIN, "python3.8"));
        }
        if (!pip3ShortcutBinary.toFile().exists()) {
            Files.createSymbolicLink(pip3ShortcutBinary,
                    Paths.get(installationPrefixDir.getAbsolutePath(), BIN, "pip3.8"));
        }
        // org.apache.commons.io.FileUtils.copyDirectory fails to copy /etc/ssl/certs/NetLock_Arany_\=Class_Gold\=_Főtanúsítvány.pem
        // due to `java.io.FileNotFoundException: /etc/ssl/certs/NetLock_Arany_=Class_Gold=_F??tan??s??tv??ny.pem (No such file or directory)`
        de.richtercloud.jhbuild.java.wrapper.FileUtils.copyFolder(Paths.get("/etc/ssl"), Paths.get(installationPrefixDir.getAbsolutePath(), "ssl"));
    }

    private boolean jhbuildDownload(String installationPrefixPath) throws InterruptedException,
            BuildFailureException,
            IOException {
        File jhbuildCloneDir = new File(downloadDir, JHBUILD_TEMPLATE);
        boolean needClone = true;
        if(jhbuildCloneDir.exists()
                && jhbuildCloneDir.list().length > 0) {
            //check whether the existing non-empty directory is a
            //valid source root
            synchronized(this) {
                if(canceled) {
                    return false;
                }
            }
            Process jhbuildSourceRootCheckProcess = createProcess(jhbuildCloneDir,
                    installationPrefixPath,
                    git,
                    "status");
            LOGGER.debug("waiting for jhbuild source root check");
            jhbuildSourceRootCheckProcess.waitFor();
            if(jhbuildSourceRootCheckProcess.exitValue() != 0) {
                OutputReaderThread stdoutReaderThread = processOutputReaderThreadMap.get(jhbuildSourceRootCheckProcess).getKey();
                OutputReaderThread stderrReaderThread = processOutputReaderThreadMap.get(jhbuildSourceRootCheckProcess).getValue();
                String stdout = REDIRECTED_TEMPLATE;
                String stderr = REDIRECTED_TEMPLATE;
                if(stdoutReaderThread != null) {
                    stdoutReaderThread.join();
                    stdout = processErrorStreamMap.get(jhbuildSourceRootCheckProcess).getLeft().toString();
                    if(outputLimit > 0) {
                        stdout = handleOutputLimit(stdout);
                    }
                }
                if(stderrReaderThread != null) {
                    stderrReaderThread.join();
                    stderr = processErrorStreamMap.get(jhbuildSourceRootCheckProcess).getRight().toString();
                }
                if(outputMode == OutputMode.JOIN_STDOUT_STDERR) {
                    throw new IllegalStateException(String.format("The "
                                    + "jhbuild clone directory '%s' already "
                                    + "exist, is not empty and is not a valid "
                                    + "git source root. This might be the "
                                    + "result of a failing previous checkout. "
                                    + "You need to check and eventually delete "
                                    + "the existing directory or specify "
                                    + "another download directory for JHBuild "
                                    + "Java wrapper (git status process had "
                                    + "output '%s').",
                            jhbuildCloneDir.getAbsolutePath(),
                            stdout));
                }else if(outputMode == OutputMode.SPLIT_STDOUT_STDERR) {
                    throw new IllegalStateException(String.format("The "
                                    + "jhbuild clone directory '%s' already "
                                    + "exist, is not empty and is not a valid "
                                    + "git source root. This might be the "
                                    + "result of a failing previous checkout. "
                                    + "You need to check and eventually delete "
                                    + "the existing directory or specify "
                                    + "another download directory for JHBuild "
                                    + "Java wrapper (git status process had "
                                    + "stdout '%s' and stderr '%s').",
                            jhbuildCloneDir.getAbsolutePath(),
                            stdout,
                            stderr));
                }else {
                    throw new IllegalArgumentException(String.format(OUTPUT_MODE_TEMPLATE,
                            outputMode));
                }
            }
            needClone = false;
        }
        if(needClone) {
            synchronized(this) {
                if(canceled) {
                    return false;
                }
            }
            Process jhbuildCloneProcess = createProcess(installationPrefixPath,
                    git,
                    "clone",
                    "https://gitlab.gnome.org/GNOME/jhbuild.git",
                    jhbuildCloneDir.getAbsolutePath());
            //directory doesn't matter because target path is
            //absolute
            LOGGER.debug("waiting for jhbuild download");
            jhbuildCloneProcess.waitFor();
            if(jhbuildCloneProcess.exitValue() != 0) {
                handleBuilderFailure(JHBUILD_TEMPLATE,
                        BuildStep.CLONE,
                        jhbuildCloneProcess);
            }
            LOGGER.debug("jhbuild download finished");
        }
        synchronized(this) {
            if(canceled) {
                return false;
            }
        }
        Process jhbuildAutogenProcess = createProcess(jhbuildCloneDir,
                installationPrefixPath,
                sh, "autogen.sh",
                String.format("--prefix=%s", installationPrefixDir.getAbsolutePath()));
        //autogen.sh runs configure
        LOGGER.debug("waiting for jhbuild build bootstrap process");
        jhbuildAutogenProcess.waitFor();
        if(jhbuildAutogenProcess.exitValue() != 0) {
            handleBuilderFailure(JHBUILD_TEMPLATE,
                    BuildStep.BOOTSTRAP,
                    jhbuildAutogenProcess);
        }
        LOGGER.debug("jhbuild build bootstrap process finished");
        synchronized(this) {
            if(canceled) {
                return false;
            }
        }
        Process jhbuildMakeProcess = createProcess(jhbuildCloneDir,
                installationPrefixPath,
                make,
                String.format("-j%d", parallelism),
                String.format("PYTHON=%s", String.join(File.separator, installationPrefixPath, BIN, PYTHON3_TEMPLATE)));
        LOGGER.debug("waiting for jhbuild build process");
        jhbuildMakeProcess.waitFor();
        if(jhbuildMakeProcess.exitValue() != 0) {
            handleBuilderFailure(JHBUILD_TEMPLATE,
                    BuildStep.MAKE,
                    jhbuildMakeProcess);
        }
        LOGGER.debug("jhbuild build process finished");
        synchronized(this) {
            if(canceled) {
                return false;
            }
        }
        Process jhbuildMakeInstallProcess = createProcess(jhbuildCloneDir,
                installationPrefixPath,
                make, "install",
                String.format("PYTHON=%s", String.join(File.separator, installationPrefixPath, BIN, PYTHON3_TEMPLATE)));
        LOGGER.debug("waiting for jhbuild installation process");
        jhbuildMakeInstallProcess.waitFor();
        if(jhbuildMakeInstallProcess.exitValue() != 0) {
            handleBuilderFailure(JHBUILD_TEMPLATE,
                    BuildStep.MAKE_INSTALL,
                    jhbuildMakeInstallProcess);
        }
        LOGGER.debug("jhbuild installation process finished");
        jhbuild = JHBUILD_TEMPLATE;
        //is found in modified path of every process built with
        //buildProcess
        LOGGER.debug(String.format("using jhbuild command '%s'",
                jhbuild));
        return true;
    }

    private void handleBuilderFailure(String moduleName,
            BuildStep buildFailureStep,
            Process failedBuildProcess) throws BuildFailureException,
            IOException,
            InterruptedException {
        String stdout = null;
        String stderr = null;
        if(stdoutOutputStream != null) {
            OutputReaderThread stdoutReaderThread = processOutputReaderThreadMap.get(failedBuildProcess).getKey();
            stdoutReaderThread.join();
            stdout = processErrorStreamMap.get(failedBuildProcess).getLeft().toString();
            if(outputLimit > 0) {
                stdout = handleOutputLimit(stdout);
            }
        }
        if(stderrOutputStream != null) {
            OutputReaderThread stderrReaderThread = processOutputReaderThreadMap.get(failedBuildProcess).getValue();
            stderrReaderThread.join();
            stderr = processErrorStreamMap.get(failedBuildProcess).getRight().toString();
        }
        if(outputMode == OutputMode.JOIN_STDOUT_STDERR) {
            throw new BuildFailureException(moduleName,
                    buildFailureStep,
                    stdout);
        }else if(outputMode == OutputMode.SPLIT_STDOUT_STDERR) {
            throw new BuildFailureException(moduleName,
                    buildFailureStep,
                    stdout,
                    stderr);
        }else {
            throw new IllegalArgumentException(String.format(OUTPUT_MODE_TEMPLATE,
                    outputMode));
        }
    }

    private String handleOutputLimit(String output) {
        if (outputLimit == 0 || output.isEmpty()) {
            return output;
        }
        if (outputLimit > output.length()) {
            return output;
        }
        int begin = output.length() - outputLimit;
        return String.format("(shortened to the last %d bytes according to specified output limit) %s",
                outputLimit,
                output.substring(begin, output.length()-1));
    }

    /**
     * Allows cancelation (with minimal delay) from another thread.
     */
    public void cancelInstallModuleset() {
        this.canceled = true;
        synchronized(this) {
            if(activeProcess != null) {
                activeProcess.destroy();
            }
        }
    }

    /**
     * Check canceled state.
     *
     * @return {@code true} if {@link #cancelInstallModuleset() } has been
     *     invoked and no other build process started so far, {@code false}
     *     otherwise
     */
    public boolean isCanceled() {
        return canceled;
    }

    /**
     * Tries to find {@code moduleName} in the default moduleset on the
     * classpath shipped with archive.
     *
     * The module installation can be canceled from another thread with
     * {@link #cancelInstallModuleset() }.
     *
     * @param moduleName the module to build
     * @return {@code true} if successful, {@code false} if canceled
     * @throws IOException if such an exception occurs
     * @throws ExtractionException if such an exception occurs
     * @throws InterruptedException if such an exception occurs
     * @throws MissingSystemBinaryException if such an exception occurs
     * @throws BuildFailureException if such an exception occurs
     * @throws ModuleBuildFailureException if such an exception occurs
     * @throws DownloadException if such an exception occurs
     */
    public boolean installModuleset(String moduleName) throws IOException,
            ExtractionException,
            InterruptedException,
            MissingSystemBinaryException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException {
        try (InputStream modulesetInputStream = JHBuildJavaWrapper.class.getResourceAsStream("/moduleset-default.xml")) {
            assert modulesetInputStream != null;
            return installModuleset(modulesetInputStream,
                    moduleName);
        }
    }

    /**
     * Installs module {@code moduleName} from JHBuild moduleset provided by
     * {@code modulesetInputStream}.
     *
     * The module installation can be canceled from another thread with
     * {@link #cancelInstallModuleset() }.
     *
     * @param modulesetInputStream the input stream containing the module
     *     information
     * @param moduleName the name of the module to build
     * @return {@code true} if successful, {@code false} if canceled
     * @throws IOException if such an exception occurs
     * @throws ExtractionException if such an exception occurs
     * @throws InterruptedException if such an exception occurs
     * @throws MissingSystemBinaryException if such an exception occurs
     * @throws BuildFailureException if such an exception occurs
     * @throws IllegalArgumentException if {@code modulesetInputStream} is
     *     {@code null}
     * @throws ModuleBuildFailureException if such an exception occurs
     * @throws DownloadException if such an exception occurs
     */
    /*
    internal implementation notes:
    - Throwing IllegalArgumentException if modulesetInputStream is null improves
    handling of InputStream which have been acquired through
    Class.getResourceAsStream since those might be null.
    - An additional check whether the module exists in the module set or not (e.g. with `jhbuild info` needs to run
    after `init` and has no advantage in terms of speed over a failure during `jhbuild build`. Tests need to deal with
    the fact that init needs to run before the check.
    */
    public boolean installModuleset(InputStream modulesetInputStream,
            String moduleName) throws IOException,
            ExtractionException,
            InterruptedException,
            MissingSystemBinaryException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException {
        canceled = false;
        if(modulesetInputStream == null) {
            throw new IllegalArgumentException("modulesetInputStream mustn't be null");
        }
        if(moduleName == null || moduleName.isEmpty()) {
            throw new IllegalArgumentException("moduleName mustn't be null or empty");
        }
        return runLockGuarded(() -> {
            String installationPrefixPath = String.join(File.pathSeparator,
                    String.join(File.separator, installationPrefixDir.getAbsolutePath(), BIN),
                    System.getenv(PATH));
            LOGGER.debug(String.format("using PATH %s for installation routines",
                    installationPrefixPath));
            boolean notCanceled = init(installationPrefixPath);
            if (!notCanceled) {
                return false;
            }
            LOGGER.debug(String.format("building module %s with jhbuild command %s",
                    moduleName,
                    jhbuild));
            String jHBuildrcTemplate = String.format("prefix=\"%s\"\n"
                            + "checkoutroot = \"%s\"",
                    installationPrefixDir.getAbsolutePath(),
                    downloadDir.getAbsolutePath());
            LOGGER.debug("jhbuild configuration file template result: '{}'",
                    jHBuildrcTemplate);
            File jHBuildrcFile = Files.createTempFile(JHBuildJavaWrapper.class.getSimpleName(), //prefix
                    "jhbuildrc" //suffix
            ).toFile();
            IOUtils.write(jHBuildrcTemplate,
                    Files.newOutputStream(jHBuildrcFile.toPath()),
                    Charsets.UTF_8);
            Process jhbuildBootstrapProcess = createProcess(installationPrefixPath,
                    jhbuild,
                    String.format("--file=%s",
                            jHBuildrcFile.getAbsolutePath()),
                    //the .jhbuildrc file
                    exitOnError ? "--exit-on-error" : "",
                    "bootstrap");
            //directory doesn't matter
            LOGGER.debug("waiting for jhbuild bootstrap process");
            jhbuildBootstrapProcess.waitFor();
            if (jhbuildBootstrapProcess.exitValue() != 0) {
                OutputReaderThread stdoutReaderThread = processOutputReaderThreadMap.get(jhbuildBootstrapProcess).getKey();
                OutputReaderThread stderrReaderThread = processOutputReaderThreadMap.get(jhbuildBootstrapProcess).getValue();
                String stdout = REDIRECTED_TEMPLATE;
                String stderr = REDIRECTED_TEMPLATE;
                if (stdoutReaderThread != null) {
                    stdoutReaderThread.join();
                    stdout = processErrorStreamMap.get(jhbuildBootstrapProcess).getLeft().toString();
                    if(outputLimit > 0) {
                        stdout = handleOutputLimit(stdout);
                    }
                }
                if (stderrReaderThread != null) {
                    stderrReaderThread.join();
                    stderr = processErrorStreamMap.get(jhbuildBootstrapProcess).getRight().toString();
                }
                if(outputMode == OutputMode.JOIN_STDOUT_STDERR) {
                    throw new ModuleBuildFailureException(String.format(JHBUILD_TEMPLATE_2
                                    + "bootstrap process returned with code %d (output was "
                                    + "'%s')",
                            jhbuildBootstrapProcess.exitValue(),
                            stdout));
                }else if(outputMode == OutputMode.SPLIT_STDOUT_STDERR) {
                    throw new ModuleBuildFailureException(String.format(JHBUILD_TEMPLATE_2
                                    + "bootstrap process returned with code %d (stdout was "
                                    + "'%s' and stderr was '%s')",
                            jhbuildBootstrapProcess.exitValue(),
                            stdout,
                            stderr));
                }else {
                    throw new IllegalArgumentException(String.format(OUTPUT_MODE_TEMPLATE,
                            outputMode));
                }
            }
            LOGGER.debug("jhbuild bootstrap process finished");
            File modulesetFile = Files.createTempFile(JHBuildJavaWrapper.class.getSimpleName(), //prefix
                    "moduleset" //suffix
            ).toFile();
            IOUtils.copy(modulesetInputStream, Files.newOutputStream(modulesetFile.toPath()));
            String conditions = null;
            if(m4BuildFailureOnCurrentOS()) {
                conditions = "+nom4";
            }
            Process jhbuildProcess = createProcess(installationPrefixPath,
                    jhbuild,
                    String.format("--file=%s",
                            jHBuildrcFile.getAbsolutePath()),
                    //the .jhbuildrc file
                    String.format("--moduleset=%s",
                            modulesetFile.getAbsolutePath()),
                    "--no-interact",
                    exitOnError ? "--exit-on-error" : "",
                    conditions != null
                            ? String.format("--conditions=\"%s\"",
                                    conditions)
                            : "",
                    "build",
                    "--nodeps",
                    moduleName);
            //directory doesn't matter
            LOGGER.debug("waiting for jhbuild build process");
            jhbuildProcess.waitFor();
            if(jhbuildProcess.exitValue() != 0) {
                OutputReaderThread stdoutReaderThread = processOutputReaderThreadMap.get(jhbuildProcess).getKey();
                OutputReaderThread stderrReaderThread = processOutputReaderThreadMap.get(jhbuildProcess).getValue();
                String stdout = REDIRECTED_TEMPLATE;
                String stderr = REDIRECTED_TEMPLATE;
                if (stdoutReaderThread != null) {
                    stdoutReaderThread.join();
                    stdout = processErrorStreamMap.get(jhbuildProcess).getLeft().toString();
                    if(outputLimit > 0) {
                        stdout = handleOutputLimit(stdout);
                    }
                }
                if (stderrReaderThread != null) {
                    stderrReaderThread.join();
                    stderr = processErrorStreamMap.get(jhbuildProcess).getRight().toString();
                }
                if(outputMode == OutputMode.JOIN_STDOUT_STDERR) {
                    throw new ModuleBuildFailureException(String.format(JHBUILD_TEMPLATE_2
                                    + "returned with code %d during building of module '%s' (output "
                                    + "was '%s')",
                            jhbuildProcess.exitValue(),
                            moduleName,
                            stdout));
                }else if(outputMode == OutputMode.SPLIT_STDOUT_STDERR) {
                    throw new ModuleBuildFailureException(String.format(JHBUILD_TEMPLATE_2
                                    + "returned with code %d during building of module '%s' (stdout "
                                    + "was '%s' and stderr was '%s')",
                            jhbuildProcess.exitValue(),
                            moduleName,
                            stdout,
                            stderr));
                }else {
                    throw new IllegalArgumentException(String.format(OUTPUT_MODE_TEMPLATE,
                            outputMode));
                }
            }
            LOGGER.debug("jhbuild build process finished");
            return true;
        });
    }

    /**
     * Handles the requirement for acquisition of both a JVM-global file lock and a lock per thread which are realized
     * with a file lock and a reentrant lock respecitvely (see
     * https://stackoverflow.com/questions/52800881/whats-the-purpose-of-waiting-in-filechannel-lock-if-overlappingfilelockexceptio/52801722#52801722
     * for details) which is necessary because mutliple instances of jhbuild running in parallel are using the same
     * cache directory in {@code HOME/.cache}.
     * @param lockGuardedLambda the lambda to be executed after acquisition of both the thread and file lock
     *     (in that order)
     * @return the return value of {@code lockGuardedLambda}
     * @throws IOException if such an exception occurs
     * @throws ExtractionException if such an exception occurs
     * @throws InterruptedException if such an exception occurs
     * @throws MissingSystemBinaryException if such an exception occurs
     * @throws BuildFailureException if such an exception occurs
     * @throws IllegalArgumentException if {@code modulesetInputStream} is
     *     {@code null}
     * @throws ModuleBuildFailureException if such an exception occurs
     * @throws DownloadException if such an exception occurs
     */
    @SuppressWarnings("PMD.AvoidFileStream")
    private boolean runLockGuarded(LockGuardedLambda lockGuardedLambda) throws IOException,
            ExtractionException,
            InterruptedException,
            MissingSystemBinaryException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException {
        JHBUILD_THREAD_LOCK.lock();
        try {
            if (!JHBUILD_FILE_LOCK_FILE.exists()) {
                Files.createDirectories(Paths.get(JHBUILD_FILE_LOCK_FILE.getParent()));
                JHBUILD_FILE_LOCK_FILE.createNewFile();
            }
            try (RandomAccessFile randomAccessFile = new RandomAccessFile(JHBUILD_FILE_LOCK_FILE,
                    "rw")) {
                //FileInputStream causes NonWritableChannelException
                try (FileLock fileLock = randomAccessFile.getChannel().lock(0, Long.MAX_VALUE, false)) {
                    try {
                        return lockGuardedLambda.action();
                    } finally {
                        fileLock.release();
                    }
                }
            }
        }finally {
            JHBUILD_THREAD_LOCK.unlock();
        }
    }

    @SuppressWarnings("PMD.UseObjectForClearerAPI")
    private String installPrerequisiteAutotools(String installationPrefixPath,
            String binary,
            String binaryDescription,
            DownloadCombi downloadCombi,
            List<DownloadCombi> patchDownloadCombis,
            int parallelism) throws IOException,
            ExtractionException,
            MissingSystemBinaryException,
            InterruptedException,
            BuildFailureException,
            DownloadException {
        return installPrerequisiteAutotools(installationPrefixPath,
                binary,
                binaryDescription,
                downloadCombi,
                patchDownloadCombis,
                parallelism,
                CONFIGURE);
    }

    @SuppressWarnings("PMD.UseObjectForClearerAPI")
    private String installPrerequisiteAutotools(String installationPrefixPath,
            String binary,
            String binaryDescription,
            DownloadCombi downloadCombi,
            List<DownloadCombi> patchDownloadCombis,
            int parallelism,
            String configure) throws IOException,
            ExtractionException,
            MissingSystemBinaryException,
            InterruptedException,
            BuildFailureException,
            DownloadException {
        return installPrerequisiteAutotools(
                installationPrefixPath,
                binary,
                binaryDescription,
                downloadCombi,
                patchDownloadCombis,
                parallelism,
                configure,
                null, //additionalConfigureOptions
                null, //additionalMakeVariables
                Collections.emptyMap()
        );
    }

    @SuppressWarnings({"PMD.UseObjectForClearerAPI",
            "PMD.UseVarargs"}) // option to ignore varargs if the previous argument is an array suggested at https://github.com/pmd/pmd/issues/346
    private String installPrerequisiteAutotools(String installationPrefixPath,
            String binary,
            String binaryDescription,
            DownloadCombi downloadCombi,
            List<DownloadCombi> patchDownloadCombis,
            int parallelism,
            String[] additionalConfigureOptions,
            String[] additionalMakeVariables,
            Map<String, String> additionalEnvConfigureVariables) throws IOException,
            ExtractionException,
            MissingSystemBinaryException,
            InterruptedException,
            BuildFailureException,
            DownloadException {
        return installPrerequisiteAutotools(installationPrefixPath,
                binary,
                binaryDescription,
                downloadCombi,
                patchDownloadCombis,
                parallelism,
                CONFIGURE,
                additionalConfigureOptions,
                additionalMakeVariables,
                additionalEnvConfigureVariables);
    }

    @SuppressWarnings({"PMD.UseObjectForClearerAPI",
        "PMD.UseVarargs"}) // option to ignore varargs if the previous argument is an array suggested at https://github.com/pmd/pmd/issues/346
    private String installPrerequisiteAutotools(String installationPrefixPath,
            String binary,
            String binaryDescription,
            DownloadCombi downloadCombi,
            List<DownloadCombi> patchDownloadCombis,
            int parallelism,
            String configure,
            String[] additionalConfigureOptions,
            String[] additionalMakeVariables,
            Map<String, String> additionalEnvConfigureVariables) throws IOException,
            ExtractionException,
            MissingSystemBinaryException,
            InterruptedException,
            BuildFailureException,
            DownloadException {
        List<BuildStepProcess> buildStepProcesses = generateBuildStepProcessesAutotools(installationPrefixPath,
                parallelism,
                configure,
                additionalConfigureOptions,
                additionalMakeVariables,
                additionalEnvConfigureVariables);
        return installPrerequisiteAutotools(
                installationPrefixPath,
                binary,
                binaryDescription,
                downloadCombi,
                patchDownloadCombis,
                buildStepProcesses);
    }

    /**
     * Installs an autotools-based prerequisiste.
     *
     * @param installationPrefixPath the {@code PATH} of the installation prefix
     *     to use
     * @param binary the binary which is supposed to be installed and returned
     *     after successful, non-canceled installation (see return value
     *     description)
     * @param binaryDescription a description of the binary to be installed used
     *     in logging messages (can be the name of the binary or a short
     *     description like "C compiler")
     * @return the path of the installed binary, the empty string in case no
     *     binary has been installed, but the installation hasn't been canceled
     *     or {@code null} if the installation or any part of it has been
     *     aborted
     * @throws IOException if such an exception occurs
     * @throws ExtractionException if such an exception occurs
     * @throws MissingSystemBinaryException if such an exception occurs
     * @throws InterruptedException if such an exception occurs
     * @throws BuildFailureException if such an exception occurs
     */
    private String installPrerequisiteAutotools(String installationPrefixPath,
            String binary,
            String binaryDescription,
            DownloadCombi downloadCombi,
            List<DownloadCombi> patchDownloadCombis,
            List<BuildStepProcess> buildStepProcesses) throws IOException,
            ExtractionException,
            MissingSystemBinaryException,
            InterruptedException,
            BuildFailureException,
            DownloadException {
        return installPrerequisite0(installationPrefixPath,
                binary,
                binaryDescription,
                downloadCombi,
                patchDownloadCombis,
                buildStepProcesses);
    }

    private String installPrerequisite0(String installationPrefixPath,
            String binary,
            String binaryDescription,
            DownloadCombi downloadCombi,
            List<DownloadCombi> patchDownloadCombis,
            List<BuildStepProcess> buildSteps) throws IOException,
            ExtractionException,
            MissingSystemBinaryException,
            InterruptedException,
            BuildFailureException,
            DownloadException {
        boolean notDownloadCanceled = downloader.downloadFile(downloadCombi,
                skipMD5SumCheck,
                DownloadFailureCallback.failAfterRetries(5),
                MD5SumCheckUnequalsCallback.fail(),
                DownloadEmptyCallback.failAfterRetries(5));
        if(!notDownloadCanceled) {
            LOGGER.debug(String.format("install prerequisiste download for %s canceled",
                    binaryDescription));
            return null;
        }
        //patching
        File extractionLocationDir = new File(downloadCombi.getExtractionLocation());
        assert extractionLocationDir.exists();
        if(patchDownloadCombis != null
                && !patchDownloadCombis.isEmpty()) {
            try {
                BinaryUtils.validateBinary(patch,
                        "patch",
                        installationPrefixPath);
            }catch(BinaryValidationException ex1) {
                throw new MissingSystemBinaryException("patch",
                        ex1);
            }
            for(DownloadCombi patchDownloadCombi : patchDownloadCombis) {
                boolean notPatchDownloadCanceled = downloader.downloadFile(patchDownloadCombi,
                        skipMD5SumCheck,
                        DownloadFailureCallback.failAfterRetries(5),
                        MD5SumCheckUnequalsCallback.fail(),
                        DownloadEmptyCallback.failAfterRetries(5));
                if(!notPatchDownloadCanceled) {
                    LOGGER.debug(String.format("install prerequisiste download for %s canceled",
                            binaryDescription));
                    return null;
                }
                assert patchDownloadCombi.getExtractionLocation() != null;
                File patchFile = new File(patchDownloadCombi.getExtractionLocation());
                if(!patchFile.isFile()) {
                    throw new IllegalArgumentException(String.format("patch "
                            + "download combi %s caused download (and eventual "
                            + "extraction) of something which is not a file",
                            patchDownloadCombi));
                }
                LOGGER.info(String.format("patching source root %s using patch "
                        + "file %s",
                        extractionLocationDir.getAbsolutePath(),
                        patchFile.getAbsolutePath()));
                Process patchProcess = createProcess(extractionLocationDir,
                        installationPrefixPath,
                        patch, "-p1", String.format("<%s",
                                patchFile.getAbsolutePath()));
                patchProcess.waitFor();
                if(patchProcess.exitValue() != 0) {
                    throw new IllegalArgumentException(String.format("patching "
                            + "extraction direction %s with patch file %s "
                            + "failed",
                            extractionLocationDir.getAbsolutePath(),
                            patchFile.getAbsolutePath()));
                }
                OutputReaderThread stdoutReaderThread = this.processOutputReaderThreadMap.get(patchProcess).getKey();
                String patchProcessStdout = REDIRECTED_TEMPLATE;
                if(stdoutReaderThread != null) {
                    stdoutReaderThread.join();
                    patchProcessStdout = processErrorStreamMap.get(patchProcess).getLeft().toString();
                }
                LOGGER.debug(String.format("successful patch process' output was: %s",
                        patchProcessStdout));
            }
        }
        //need make for building and it's overly hard to bootstrap
        //without it, so force installation out of JHBuild wrapper
        try {
            BinaryUtils.validateBinary(make,
                    MAKE_BINARY_NAME,
                    installationPrefixPath);
        }catch(BinaryValidationException ex1) {
            throw new MissingSystemBinaryException(MAKE_BINARY_NAME,
                    ex1);
        }
        //build
        synchronized(this) {
            if(canceled) {
                LOGGER.debug(String.format("canceling prerequisiste installation of %s because the build wrapper has been canceled",
                        binaryDescription));
                return null;
            }
        }
        for(BuildStepProcess buildProcessStep : buildSteps) {
            Process process = buildProcessStep.getProcess(extractionLocationDir);
            process.waitFor();
            if(process.exitValue() != 0) {
                BuildStep buildStep = buildProcessStep.getBuildStep();
                handleBuilderFailure(binaryDescription,
                        buildStep,
                        process);
            }
            synchronized(this) {
                if(canceled) {
                    LOGGER.debug(String.format("canceling prerequisiste installation of %s because the build wrapper has been canceled",
                            binaryDescription));
                    return null;
                }
            }
        }
        return binary;
            //is found in modified path of every process built with
            //buildProcess
    }

    /**
     * Checks presence of a {@code .pc} file in the specified installation
     * prefix. If it's not present in the installation prefix searches in the whole system.
     *
     * @param installationPrefixDir the installation prefix directory
     * @param pcFileName the name of the {@code .pc} file to search for
     * @return {@code true} if the file has been found, {@code false} otherwise
     * @throws IOException if {@link Files#walk(Path, FileVisitOption...)} throws such an exception
     * @throws LibraryNotPresentException if the library to check is not present on the system
     */
    protected Path checkLibPresence(File installationPrefixDir,
            String pcFileName) throws IOException,
            LibraryNotPresentException {
        Optional<Path> hit = Files.walk(installationPrefixDir.toPath())
                .filter(file -> file.getFileName().toFile().getName().equals(pcFileName))
                .findAny();
            //recursive search, from
            //https://stackoverflow.com/questions/10780747/recursively-search-for-a-directory-in-java
        if(!hit.isPresent()) {
            hit = Stream.of(File.listRoots()).parallel()
                    .map(root -> {
                        try {
                            return Files.walk(Paths.get(root.getName()))
                                    .filter(file -> file.getFileName().toFile().getName().equals(pcFileName))
                                    .findAny();
                        } catch (IOException e) {
                            throw new UncheckedIOException(e);
                        }
                    })
                    .filter(root -> root.isPresent())
                    .map(root -> root.get())
                    .findAny();
        }
        if(!hit.isPresent()) {
            throw new LibraryNotPresentException();
        }
        return hit.get();
    }

    private List<BuildStepProcess> generateBuildStepProcessesAutotools(final String installationPrefixPath,
            final int parallelism,
            final String configureName) {
        return generateBuildStepProcessesAutotools(installationPrefixPath,
                parallelism,
                configureName,
                null, //additionalConfigureOptions
                null, //additionalMakeVariables
                Collections.emptyMap()
        );
    }

    /**
     * Generates the list of {@link BuildStepProcess}s in order to allow them to
     * be overridden for non-default configuration or build processes (e.g.
     * {@code libxml2} which requires {@code --with-python-install-dir=DIR} to
     * be passed to {@code configure} in order to make installation without
     * {@code root} privileges work.
     *
     * @param installationPrefixPath the installation prefix path to pass to all
     *     processes environment
     * @param additionalConfigureOptions additional {@code configure} options
     *     passed after the necessary {@code --prefix}
     * @param additionalMakeVariables additional {@code make} variables passed after {@code }
     * @return the list of generated build step processes
     */
    @SuppressWarnings({"PMD.UseObjectForClearerAPI",
        "PMD.UseVarargs"}) // option to ignore varargs if the previous argument is an array suggested at https://github.com/pmd/pmd/issues/346
    private List<BuildStepProcess> generateBuildStepProcessesAutotools(final String installationPrefixPath,
            final int parallelism,
            final String configureName,
            final String[] additionalConfigureOptions,
            final String[] additionalMakeVariables,
            final Map<String, String> additionalEnvConfigureVariables) {
        assert parallelism >= 1: String.format("parallelism has to be >= 1 in "
                + "order to make sense (was %s)",
                parallelism);
        return new LinkedList<>(Arrays.asList(new BuildStepProcess() {
            @Override
            public Process getProcess(File extractionLocationDir) throws IOException {
                List<String> commandList = new LinkedList<>(Arrays.asList(sh, configureName,
                        String.format("--prefix=%s", installationPrefixDir.getAbsolutePath())));
                if(additionalConfigureOptions != null) {
                    for (String additionalConfigureOption : additionalConfigureOptions) {
                        commandList.add(additionalConfigureOption);
                    }
                }
                String[] commands = commandList.toArray(new String[0]);
                Process configureProcess = createProcess(extractionLocationDir,
                        ImmutableMap.<String, String>builder()
                                .put(PATH, installationPrefixPath)
                                .put("CPPFLAGS", String.format("-I%s",
                                        new File(installationPrefixDir, INCLUDE).getAbsolutePath()))
                                .put("CFLAGS", String.format("-I%s -L%s -L%s",
                                        new File(installationPrefixDir, INCLUDE).getAbsolutePath(),
                                        new File(installationPrefixDir, LIB).getAbsolutePath(),
                                        new File(installationPrefixDir, LIB64).getAbsolutePath()))
                                .put("CXXFLAGS", String.format("-I%s -L%s -L%s",
                                        new File(installationPrefixDir, INCLUDE).getAbsolutePath(),
                                        new File(installationPrefixDir, LIB).getAbsolutePath(),
                                        new File(installationPrefixDir, LIB64).getAbsolutePath()))
                                .put("LDFLAGS", String.format("-L%s -L%s  -Wl,-R%s -Wl,-R%s",
                                        new File(installationPrefixDir, LIB).getAbsolutePath(),
                                        new File(installationPrefixDir, LIB64).getAbsolutePath(),
                                        new File(installationPrefixDir, LIB).getAbsolutePath(),
                                        new File(installationPrefixDir, LIB64).getAbsolutePath()))
                                .putAll(additionalEnvConfigureVariables)
                                .build(),
                        commands);
                return configureProcess;
            }

            @Override
            public BuildStep getBuildStep() {
                return BuildStep.CONFIGURE;
            }
        },
                new BuildStepProcess() {
                    @Override
                    public Process getProcess(File extractionLocationDir) throws IOException {
                        List<String> commandList = new LinkedList<>(Arrays.asList(make, String.format("-j%d", parallelism)));
                        if(additionalMakeVariables != null) {
                            for (String additionalMakeVariable : additionalMakeVariables) {
                                commandList.add(additionalMakeVariable);
                            }
                        }
                        String[] commands = commandList.toArray(new String[0]);
                        return createProcess(extractionLocationDir,
                                ImmutableMap.<String, String>builder()
                                        .put(PATH, installationPrefixPath)
                                        .put("CFLAGS", String.format("-I%s -L%s", new File(installationPrefixDir, INCLUDE).getAbsolutePath(),
                                                new File(installationPrefixDir, LIB).getAbsolutePath()))
                                        .build(),
                                commands);
                    }

                    @Override
                    public BuildStep getBuildStep() {
                        return BuildStep.MAKE;
                    }
                },
                new BuildStepProcess() {
                    @Override
                    public Process getProcess(File extractionLocationDir) throws IOException {
                        return createProcess(extractionLocationDir,
                                installationPrefixPath,
                                make,
                                "install");
                    }

                    @Override
                    public BuildStep getBuildStep() {
                        return BuildStep.MAKE_INSTALL;
                    }
                }));
    }

    private interface BuildStepProcess {

        Process getProcess(File extractionLocationDir) throws IOException;

        BuildStep getBuildStep();
    }

    @FunctionalInterface
    private interface LockGuardedLambda {
        boolean action() throws IOException,
                ExtractionException,
                InterruptedException,
                MissingSystemBinaryException,
                BuildFailureException,
                ModuleBuildFailureException,
                DownloadException;
    }

    /**
     * Used to avoid returning {@code null} from {@link #checkLibPresence(File, String)}.
     */
    protected class LibraryNotPresentException extends Exception {
        private static final long serialVersionUID = 1L;
    }
}
