/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */
package de.richtercloud.jhbuild.wrapper.it;

import de.richtercloud.jhbuild.java.wrapper.ActionOnMissingBinary;
import de.richtercloud.jhbuild.java.wrapper.BuildFailureException;
import de.richtercloud.jhbuild.java.wrapper.JHBuildJavaWrapper;
import de.richtercloud.jhbuild.java.wrapper.MissingSystemBinaryException;
import de.richtercloud.jhbuild.java.wrapper.ModuleBuildFailureException;
import de.richtercloud.jhbuild.java.wrapper.OutputMode;
import de.richtercloud.jhbuild.java.wrapper.download.AutoDownloader;
import de.richtercloud.jhbuild.java.wrapper.download.DownloadException;
import de.richtercloud.jhbuild.java.wrapper.download.Downloader;
import de.richtercloud.jhbuild.java.wrapper.download.ExtractionException;
import static de.richtercloud.jhbuild.wrapper.it.TestUtils.SEPARATE_IT_NAME_PROPERTY_NAME;
import static de.richtercloud.jhbuild.wrapper.it.TestUtils.SEPARATE_IT_PROPERTY_NAME;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Tests all root modules.
 */
public class ModulesetIT {
    private final static Logger LOGGER = LoggerFactory.getLogger(ModulesetIT.class);
    /**
     * Reflect changes to the test method name automatically. There seems to be no easy way to print the test method
     * name before start although the realization would be rather trivial. Needs to be {@code public}.
     */
    @Rule
    public TestName name = new TestName();

    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testPostgresql105() throws IOException,
            InterruptedException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException,
            MissingSystemBinaryException,
            ExtractionException {
        if(System.getProperty(SEPARATE_IT_PROPERTY_NAME) != null
                && !name.getMethodName().equals(System.getProperty(SEPARATE_IT_NAME_PROPERTY_NAME))) {
            return;
        }
        LOGGER.info(name.getMethodName());
        moduleSetTest("postgresql-10.5");
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testPostgresql96() throws IOException,
            InterruptedException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException,
            MissingSystemBinaryException,
            ExtractionException {
        if(System.getProperty(SEPARATE_IT_PROPERTY_NAME) != null
                && !name.getMethodName().equals(System.getProperty(SEPARATE_IT_NAME_PROPERTY_NAME))) {
            return;
        }
        LOGGER.info(name.getMethodName());
        moduleSetTest("postgresql-9.6.17");
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testPostgresql95() throws IOException,
            InterruptedException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException,
            MissingSystemBinaryException,
            ExtractionException {
        if(System.getProperty(SEPARATE_IT_PROPERTY_NAME) != null
                && !name.getMethodName().equals(System.getProperty(SEPARATE_IT_NAME_PROPERTY_NAME))) {
            return;
        }
        LOGGER.info(name.getMethodName());
        moduleSetTest("postgresql-9.5.21");
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testMysql57() throws IOException,
            InterruptedException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException,
            MissingSystemBinaryException,
            ExtractionException {
        if(System.getProperty(SEPARATE_IT_PROPERTY_NAME) != null
                && !name.getMethodName().equals(System.getProperty(SEPARATE_IT_NAME_PROPERTY_NAME))) {
            return;
        }
        LOGGER.info(name.getMethodName());
        moduleSetTest("mysql-5.7.28");
    }

    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testMysql56() throws IOException,
            InterruptedException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException,
            MissingSystemBinaryException,
            ExtractionException {
        if(System.getProperty(SEPARATE_IT_PROPERTY_NAME) != null
                && !name.getMethodName().equals(System.getProperty(SEPARATE_IT_NAME_PROPERTY_NAME))) {
            return;
        }
        LOGGER.info(name.getMethodName());
        moduleSetTest("mysql-5.6.46");
    }

    /**
     * Only executed on CI which allows parallelization in different containers in order to be able to detect module
     * failure as early as possible (doesn't make sense on the same machine/in the same container because tests are
     * queued because of the jhbuild cache directory lock and dependency modules are covered by entrypoint modules
     * anyway).
     * @throws IOException not expected
     * @throws InterruptedException not expected
     * @throws BuildFailureException not expected
     * @throws ModuleBuildFailureException not expected
     * @throws DownloadException not expected
     * @throws MissingSystemBinaryException not expected
     * @throws ExtractionException not expected
     */
    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testReadline70() throws IOException,
            InterruptedException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException,
            MissingSystemBinaryException,
            ExtractionException {
        if(System.getProperty(SEPARATE_IT_PROPERTY_NAME) == null
                || !name.getMethodName().equals(System.getProperty(SEPARATE_IT_NAME_PROPERTY_NAME))) {
            return;
        }
        LOGGER.info(name.getMethodName());
        moduleSetTest("readline-7.0");
    }

    /**
     * Only executed on CI which allows parallelization in different containers in order to be able to detect module
     * failure as early as possible (doesn't make sense on the same machine/in the same container because tests are
     * queued because of the jhbuild cache directory lock and dependency modules are covered by entrypoint modules
     * anyway).
     * @throws IOException not expected
     * @throws InterruptedException not expected
     * @throws BuildFailureException not expected
     * @throws ModuleBuildFailureException not expected
     * @throws DownloadException not expected
     * @throws MissingSystemBinaryException not expected
     * @throws ExtractionException not expected
     */
    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testReadline63() throws IOException,
            InterruptedException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException,
            MissingSystemBinaryException,
            ExtractionException {
        if(System.getProperty(SEPARATE_IT_PROPERTY_NAME) == null
                || !name.getMethodName().equals(System.getProperty(SEPARATE_IT_NAME_PROPERTY_NAME))) {
            return;
        }
        LOGGER.info(name.getMethodName());
        moduleSetTest("readline-6.3");
    }

    /**
     * Only executed on CI which allows parallelization in different containers in order to be able to detect module
     * failure as early as possible (doesn't make sense on the same machine/in the same container because tests are
     * queued because of the jhbuild cache directory lock and dependency modules are covered by entrypoint modules
     * anyway).
     * @throws IOException not expected
     * @throws InterruptedException not expected
     * @throws BuildFailureException not expected
     * @throws ModuleBuildFailureException not expected
     * @throws DownloadException not expected
     * @throws MissingSystemBinaryException not expected
     * @throws ExtractionException not expected
     */
    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testNcurses61() throws IOException,
            InterruptedException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException,
            MissingSystemBinaryException,
            ExtractionException {
        if(System.getProperty(SEPARATE_IT_PROPERTY_NAME) == null
                || !name.getMethodName().equals(System.getProperty(SEPARATE_IT_NAME_PROPERTY_NAME))) {
            return;
        }
        LOGGER.info(name.getMethodName());
        moduleSetTest("ncurses-6.1");
    }

    /**
     * Only executed on CI which allows parallelization in different containers in order to be able to detect module
     * failure as early as possible (doesn't make sense on the same machine/in the same container because tests are
     * queued because of the jhbuild cache directory lock and dependency modules are covered by entrypoint modules
     * anyway).
     * @throws IOException not expected
     * @throws InterruptedException not expected
     * @throws BuildFailureException not expected
     * @throws ModuleBuildFailureException not expected
     * @throws DownloadException not expected
     * @throws MissingSystemBinaryException not expected
     * @throws ExtractionException not expected
     */
    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testFlex263() throws IOException,
            InterruptedException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException,
            MissingSystemBinaryException,
            ExtractionException {
        if(System.getProperty(SEPARATE_IT_PROPERTY_NAME) == null
                || !name.getMethodName().equals(System.getProperty(SEPARATE_IT_NAME_PROPERTY_NAME))) {
            return;
        }
        LOGGER.info(name.getMethodName());
        moduleSetTest("flex-2.6.3");
    }

    /**
     * Only executed on CI which allows parallelization in different containers in order to be able to detect module
     * failure as early as possible (doesn't make sense on the same machine/in the same container because tests are
     * queued because of the jhbuild cache directory lock and dependency modules are covered by entrypoint modules
     * anyway).
     * @throws IOException not expected
     * @throws InterruptedException not expected
     * @throws BuildFailureException not expected
     * @throws ModuleBuildFailureException not expected
     * @throws DownloadException not expected
     * @throws MissingSystemBinaryException not expected
     * @throws ExtractionException not expected
     */
    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testM41417() throws IOException,
            InterruptedException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException,
            MissingSystemBinaryException,
            ExtractionException {
        if(System.getProperty(SEPARATE_IT_PROPERTY_NAME) == null
                || !name.getMethodName().equals(System.getProperty(SEPARATE_IT_NAME_PROPERTY_NAME))) {
            return;
        }
        if (JHBuildJavaWrapper.m4BuildFailureOnCurrentOS()) {
            return;
        }
        LOGGER.info(name.getMethodName());
        moduleSetTest("m4-1.4.17");
    }

    /**
     * Only executed on CI which allows parallelization in different containers in order to be able to detect module
     * failure as early as possible (doesn't make sense on the same machine/in the same container because tests are
     * queued because of the jhbuild cache directory lock and dependency modules are covered by entrypoint modules
     * anyway).
     * @throws IOException not expected
     * @throws InterruptedException not expected
     * @throws BuildFailureException not expected
     * @throws ModuleBuildFailureException not expected
     * @throws DownloadException not expected
     * @throws MissingSystemBinaryException not expected
     * @throws ExtractionException not expected
     */
    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testBison31() throws IOException,
            InterruptedException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException,
            MissingSystemBinaryException,
            ExtractionException {
        if(System.getProperty(SEPARATE_IT_PROPERTY_NAME) == null
                || !name.getMethodName().equals(System.getProperty(SEPARATE_IT_NAME_PROPERTY_NAME))) {
            return;
        }
        LOGGER.info(name.getMethodName());
        moduleSetTest("bison-3.1");
    }

    /**
     * Only executed on CI which allows parallelization in different containers in order to be able to detect module
     * failure as early as possible (doesn't make sense on the same machine/in the same container because tests are
     * queued because of the jhbuild cache directory lock and dependency modules are covered by entrypoint modules
     * anyway).
     * @throws IOException not expected
     * @throws InterruptedException not expected
     * @throws BuildFailureException not expected
     * @throws ModuleBuildFailureException not expected
     * @throws DownloadException not expected
     * @throws MissingSystemBinaryException not expected
     * @throws ExtractionException not expected
     */
    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testCmake3123() throws IOException,
            InterruptedException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException,
            MissingSystemBinaryException,
            ExtractionException {
        if(System.getProperty(SEPARATE_IT_PROPERTY_NAME) == null
                || !name.getMethodName().equals(System.getProperty(SEPARATE_IT_NAME_PROPERTY_NAME))) {
            return;
        }
        LOGGER.info(name.getMethodName());
        moduleSetTest("cmake-3.12.3");
    }

    /**
     * Only executed on CI which allows parallelization in different containers in order to be able to detect module
     * failure as early as possible (doesn't make sense on the same machine/in the same container because tests are
     * queued because of the jhbuild cache directory lock and dependency modules are covered by entrypoint modules
     * anyway).
     * @throws IOException not expected
     * @throws InterruptedException not expected
     * @throws BuildFailureException not expected
     * @throws ModuleBuildFailureException not expected
     * @throws DownloadException not expected
     * @throws MissingSystemBinaryException not expected
     * @throws ExtractionException not expected
     */
    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testBoost1590() throws IOException,
            InterruptedException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException,
            MissingSystemBinaryException,
            ExtractionException {
        if(System.getProperty(SEPARATE_IT_PROPERTY_NAME) == null
                || !name.getMethodName().equals(System.getProperty(SEPARATE_IT_NAME_PROPERTY_NAME))) {
            return;
        }
        LOGGER.info(name.getMethodName());
        moduleSetTest("boost-1.59.0");
    }

    /**
     * Only executed on CI which allows parallelization in different containers in order to be able to detect module
     * failure as early as possible (doesn't make sense on the same machine/in the same container because tests are
     * queued because of the jhbuild cache directory lock and dependency modules are covered by entrypoint modules
     * anyway).
     * @throws IOException not expected
     * @throws InterruptedException not expected
     * @throws BuildFailureException not expected
     * @throws ModuleBuildFailureException not expected
     * @throws DownloadException not expected
     * @throws MissingSystemBinaryException not expected
     * @throws ExtractionException not expected
     */
    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testPython2715() throws IOException,
            InterruptedException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException,
            MissingSystemBinaryException,
            ExtractionException {
        if(System.getProperty(SEPARATE_IT_PROPERTY_NAME) == null
                || !name.getMethodName().equals(System.getProperty(SEPARATE_IT_NAME_PROPERTY_NAME))) {
            return;
        }
        LOGGER.info(name.getMethodName());
        moduleSetTest("python-2.7.15");
    }

    /**
     * Only executed on CI which allows parallelization in different containers in order to be able to detect module
     * failure as early as possible (doesn't make sense on the same machine/in the same container because tests are
     * queued because of the jhbuild cache directory lock and dependency modules are covered by entrypoint modules
     * anyway).
     * @throws IOException not expected
     * @throws InterruptedException not expected
     * @throws BuildFailureException not expected
     * @throws ModuleBuildFailureException not expected
     * @throws DownloadException not expected
     * @throws MissingSystemBinaryException not expected
     * @throws ExtractionException not expected
     */
    @Test
    @SuppressWarnings("PMD.JUnitTestsShouldIncludeAssert")
    public void testBzip2106() throws IOException,
            InterruptedException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException,
            MissingSystemBinaryException,
            ExtractionException {
        if(System.getProperty(SEPARATE_IT_PROPERTY_NAME) == null
                || !name.getMethodName().equals(System.getProperty(SEPARATE_IT_NAME_PROPERTY_NAME))) {
            return;
        }
        LOGGER.info(name.getMethodName());
        moduleSetTest("bzip2-1.0.6");
    }

    private void moduleSetTest(String moduleName) throws IOException,
            InterruptedException,
            BuildFailureException,
            ModuleBuildFailureException,
            DownloadException,
            MissingSystemBinaryException,
            ExtractionException {
        File installationPrefixDir = Files.createTempDirectory(String.format("%s-prefix",
                ModulesetIT.class.getSimpleName())).toFile();
        LOGGER.debug(String.format("installation prefix directory: %s",
                installationPrefixDir.getAbsolutePath()));
        File downloadDir = Files.createTempDirectory(String.format("%s-download",
                ModulesetIT.class.getSimpleName())).toFile();
        LOGGER.debug(String.format("download directory: %s",
                downloadDir.getAbsolutePath()));
        Downloader downloader = new AutoDownloader();
        JHBuildJavaWrapper jHBuildJavaWrapper = new JHBuildJavaWrapper(installationPrefixDir,
                downloadDir,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                ActionOnMissingBinary.DOWNLOAD,
                downloader, //downloader
                false, //skipMD5SumCheck
                new ByteArrayOutputStream(), //stdoutOutputStream (saves log
                //capacity on Travis CI service with 4MB log size limit and
                //doesn't hurt because stdout and stderr are printed in
                //BuildFailureException message)
                new ByteArrayOutputStream(), //stderrOutputStream
                OutputMode.JOIN_STDOUT_STDERR,
                4_194_304/16,
                true
        );
        jHBuildJavaWrapper.installModuleset(moduleName);
    }
}
